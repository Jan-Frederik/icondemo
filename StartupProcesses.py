'''
Created on Feb 2, 2017

@author: mb @ pozyx labs
'''

from algorithms.positioningserver.tdoafilereader import TdoaJsonReader
from algorithms.positioningserver.tdoaprocessor import TdoaProcessor
from multiprocessing import Process, Queue
import sys
import os
import time
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from gui.TDOAGui import TdoaMainWindow
from preprocessor.dispatcher import run_dispatcher

from utilities.queue_exporter import run_queue_exporter
from utilities.queue_importer import run_queue_importer


import socket
import struct

def set_sync_period(sync_period):
    value = int(249600000 * sync_period) #second = 249600000
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    data = struct.pack("BBHI",1,7, 26,value)
    s.sendto(data, ("192.168.100.1",7777))

def set_number_of_tags(number_of_tags):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    data = struct.pack("BBHB", 1, 7, 38, number_of_tags)
    s.sendto(data, ("192.168.100.1", 7777))






def print_out(osq, obq):
    while True:
        if not osq.empty():
            print(osq.get())
        elif not obq.empty():
            print(obq.get())

def spawnGui(taq_queue, sync_queue, parameter_queue):
    app = QApplication(sys.argv)
    ex = TdoaMainWindow(taq_queue, sync_queue, parameter_queue)
    ex.show()
    sys.exit(app.exec_())


def spawnTdoaProcessor(in_sync_queue, out_sync_queue, in_blink_queue, out_blink_queue, parameter_queue):
    tp = TdoaProcessor(in_sync_queue, out_sync_queue,
                       in_blink_queue, out_blink_queue,
                       parameter_queue)
    tp.start()

if __name__ == '__main__':
    import socket
    in_sync_queue = Queue()
    in_blink_queue = Queue()
    out_sync_queue = Queue()
    out_blink_queue = Queue()
    parameter_queue = Queue()

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = ('0.0.0.0', 8888)
    s.bind(server_address)

    sync_periods = [0.2]#[0.1, 0.2, 0.5, 1.0, 2.0, 5.0
    #meas_time = 2.0*60

    for sp in sync_periods:

        set_sync_period(sp)
        time.sleep(0.5)
        set_sync_period(sp)
        time.sleep(0.5)
        set_sync_period(sp)
        time.sleep(0.5)
        set_sync_period(sp)
        time.sleep(0.5)
        set_sync_period(sp)
        print(sp)
        sp_name = ("{:.1f}".format(sp)).replace('.','_')
        print(sp_name)

        p1 = Process(target=run_dispatcher, args=(s, in_sync_queue, in_blink_queue))
        #p1_1 = Process(target=run_queue_importer, args=(in_sync_queue, 'test_in_sync'))
        #p1_2 = Process(target=run_queue_importer, args=(in_blink_queue, 'test_in_blink'))

        #p2_1 = Process(target=run_queue_exporter, args=(meas_time, in_sync_queue, 'kunst_'+sp_name+'_sync'))
        #p2_2 = Process(target=run_queue_exporter, args=(meas_time, in_blink_queue, 'kunst_'+sp_name+'_blink'))
        p2 = Process(target=spawnTdoaProcessor, args=(in_sync_queue, out_sync_queue, in_blink_queue, out_blink_queue, parameter_queue))
        #p2_1 = Process(target=run_queue_importer, args=(out_sync_queue, 'test_out_sync'))
        #p2_2 = Process(target=run_queue_importer, args=(out_blink_queue, 'test_out_blink'))

        #p3_1 = Process(target=run_queue_exporter, args=(2.*60, out_sync_queue, 'test_out_sync'))
        #p3_2 = Process(target=run_queue_exporter, args=(2.*60, out_blink_queue, 'test_out_blink'))
        p3 = Process(target=spawnGui, args=(out_blink_queue, out_sync_queue, parameter_queue))

        p3.start()
        print('GUI started')
        #p3_1.start()
        #p3_2.start()
        #print('queue exporters started')
        #p2_1.start()
        #p2_2.start()
        print('queue importers started')
        p2.start()
        #print('processor started')
        #p2_1.start()
        #p2_2.start()
        #print('queue exporters started')
        #p1_1.start()
        #p1_2.start()
        #print('queue importers started')
        p1.start()
        print('in reader started')

        #p2_1.join(meas_time+10)
        #if p2_1.is_alive():
        #    p2_1.terminate()
        #p2_2.join(meas_time+10)
        #if p2_2.is_alive():
        #    p2_2.terminate()
        #p1.join(meas_time+10)
        #if p1.is_alive():
        #    p1.terminate()
