'''
Created on Jan 26, 2017

@author: mb @ pozyx labs
'''

import numpy as np

def euclid_dist(loc1, loc2):
    return ( (loc1[0]-loc2[0])**2 + (loc1[1]-loc2[1])**2 + (loc1[2]-loc2[2])**2 )**0.5

class Anchor:

    def __init__(self, loc=(0,0,0), clock=(1.000000000,0)):
        self.loc = loc
        self.clock = clock
        self.clock_dev = 0
        
    def get_range(self, anchor2):
        return euclid_dist(self.loc,anchor2.loc)
        
    def set_clock_random(self, std_dev=(3e-8,24*60*60*3e11,1.5e-10*3e11)):#
        self.clock = (np.random.normal(1.000000000,std_dev[0]),np.random.normal(0,std_dev[1]))
        self.clock_dev = std_dev[2]
        
    def twr_range(self, anchor2):
        return (self.get_range(anchor2)*(self.clock[0]+anchor2.clock[0])/2)
    
    def get_clock_time(self, real_time):
        c = self.clock[0]*real_time + self.clock[1]
        if self.clock_dev>0:
            c += np.random.normal(0,self.clock_dev)
        return c