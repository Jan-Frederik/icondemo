'''
Created on Jan 27, 2017

@author: mb @ pozyx labs
'''

from distsync.anchor import Anchor

class Geometry:

    def __init__(self, anchor_locs = [(2000,2000,2000),(10000,2000,2001),(2000,7500,2002),(10000,7500,2003)]):
        self.anchor_list = []
        for anchor_loc in anchor_locs:
            anchor = Anchor(anchor_loc)
            anchor.set_clock_random()
            self.anchor_list.append(anchor)
        