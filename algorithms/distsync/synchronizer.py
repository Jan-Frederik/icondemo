'''
Created on Jan 26, 2017

@author: mb @ pozyx labs
'''

import numpy as np

def solve_least_squares(amat, bvec):
    try:
        amatt = amat.transpose()
        least_squares_solution = np.dot( np.dot( np.linalg.inv( np.dot( amatt, amat ) ), amatt), bvec)
        return least_squares_solution
    except np.linalg.linalg.LinAlgError:
        return None

class Synchronizer:

    def __init__(self, meas_distances, conversion=3e11/(128*499.2e6), max_timestamp=1099511627776):
        self.n_anchors = meas_distances.shape[0]
        self.meas_distances = meas_distances

        self.conversion = conversion
        self.max_timestamp = max_timestamp

        self.timestamp_list = [[0,0] for i in range(self.n_anchors)] #last timestamp and current correction for each anchor

        self.a_list = []
        self.a_last = []
        self.gen = 0

        self.desync = np.zeros(meas_distances.shape)

        self.correction1 = np.ones((self.n_anchors,1))
        self.correction2 = np.zeros((self.n_anchors,1))
        self.correction_list = [[1,0] for i in range(self.n_anchors)] #rico correction and offset for each anchor

    def update_timestamp_constants(self, anchor_id, timestamp):
        if timestamp < (self.timestamp_list[anchor_id][0]-1e12):
            if anchor_id == (self.n_anchors-1):     # change the overall time
                #print('HELP '+str(anchor_id)+': '+str(timestamp)+' - '+str(self.timestamp_list[anchor_id][0]))
                # change all max-timestep-offsets
                for ai in range(self.n_anchors):
                    if not ai==anchor_id:
                        self.timestamp_list[ai][1]-=1
                # change saved beacons
                self.a_list = [[a[0],a[1]-self.max_timestamp*self.conversion,a[2],a[3]-self.max_timestamp*self.conversion,a[4]] for a in self.a_list]
                self.a_last = [[a[0],a[1]-self.max_timestamp*self.conversion,a[2],a[3]-self.max_timestamp*self.conversion,a[4]] for a in self.a_last]
                # change conversion
                self.correction_list = [[c[0],c[1]-c[0]*self.max_timestamp*self.conversion] for c in self.correction_list]
            else:                                   # keep the overall time
                #print('help '+str(anchor_id)+': '+str(timestamp)+' - '+str(self.timestamp_list[anchor_id][0]))
                self.timestamp_list[anchor_id][1]+=1
            self.timestamp_list[anchor_id][0] = timestamp
        elif (self.timestamp_list[anchor_id][0]+1e12) > timestamp:
            self.timestamp_list[anchor_id][0] = timestamp

    def convert_timestamp(self, anchor_id, timestamp):
        return (timestamp+self.timestamp_list[anchor_id][1]*self.max_timestamp)*self.conversion

    def load_beacons(self, beacons):
        self.a_last = []
        for b in beacons:
            self.update_timestamp_constants(b[0],b[1])
            self.update_timestamp_constants(b[2],b[3])
        for b in beacons:
            cb = [b[0],self.convert_timestamp(b[0],b[1]),b[2],self.convert_timestamp(b[2],b[3]),self.gen]
            self.desync[b[0],b[2]] = self.meas_distances[cb[0],cb[2]]-(self.correction_list[cb[2]][0]*cb[3]-self.correction_list[cb[2]][1]-self.correction_list[cb[0]][0]*cb[1]+self.correction_list[cb[0]][1])
            self.a_list.append(cb)
            self.a_last.append(cb)
        #print(self.desync[0,:])
        self.gen += 1

    def forget_beacons(self, forget_g=2):
        new_a_list = []
        for a in self.a_list:
            if a[4]>=(self.gen-forget_g):
                new_a_list.append(a)
        self.a_list = new_a_list

    def synchronize(self):
        # construct matrices for rate correction
        gamma_mat = []
        d_gamma_vec = []
        for a in self.a_list:
            for a_old in self.a_list:
                if a_old[4]==(a[4]-1) and a_old[0]==a[0] and a_old[2]==a[2]:
                    (a1i,a1t, a2i,a2t, g) = a
                    (a1i,a1t_old, a2i,a2t_old, g) = a_old
                    gamma_row = [0 for i in range(self.n_anchors-1)]
                    d_gamma_row = [0]
                    a1td = a1t-a1t_old
                    a2td = a2t-a2t_old
                    if a1i==self.n_anchors-1:
                        gamma_row = [g+a1td for g in gamma_row]
                        d_gamma_row = [self.n_anchors*a1td]
                    else:
                        gamma_row[a1i] -= a1td
                    if a2i==self.n_anchors-1:
                        gamma_row = [g-a2td for g in gamma_row]
                        d_gamma_row = [-self.n_anchors*a2td]
                    else:
                        gamma_row[a2i] += a2td
                    gamma_mat.append(gamma_row)
                    d_gamma_vec.append(d_gamma_row)
        correction1 = solve_least_squares(np.array(gamma_mat), np.array(d_gamma_vec))
        if correction1 is None:
            print('singular matrix: no update of sync')
            return 'singular matrix: no update of sync'
        else:
            self.correction1 = np.vstack((correction1, self.n_anchors-sum(correction1[:,0])))
        # construct matrices for offset correction
        t0_mat = []
        d_vec = []
        gamma_vec = []
        for a in self.a_last:
            (a1i,a1t, a2i,a2t, t) = a
            if self.meas_distances[a1i,a2i]>0:
                t0_row = [0 for i in range(self.n_anchors-1)]
                d_row = [self.meas_distances[a1i,a2i]]
                gamma_row = [- self.correction1[a2i,0]*a2t + self.correction1[a1i,0]*a1t]
                if not a1i==self.n_anchors-1:
                    t0_row[a1i] = 1
                if not a2i==self.n_anchors-1:
                    t0_row[a2i] = -1
                t0_mat.append(t0_row)
                d_vec.append(d_row)
                gamma_vec.append(gamma_row)
        correction2 = solve_least_squares(np.array(t0_mat), np.array(d_vec)+np.array(gamma_vec))
        if correction2 is None:
            print('singular matrix: no update of sync')
            return 'singular matrix: no update of sync'
        else:
            self.correction2 = np.vstack((correction2, 0))
        # set correction
        for i in range(self.n_anchors):
            self.correction_list[i] = [self.correction1[i,0], self.correction2[i,0]]

    def calc_sync(self, beacons, forget_g=10):
        self.load_beacons(beacons)
        self.forget_beacons(forget_g=forget_g)
        self.synchronize()

    def correct_times(self, ts_uncorrected):
        for t_uncorrected in ts_uncorrected:
            self.update_timestamp_constants(t_uncorrected[0],t_uncorrected[1])
        return [[t_uncorrected[0], self.correction_list[t_uncorrected[0]][0]*self.convert_timestamp(t_uncorrected[0],t_uncorrected[1])-self.correction_list[t_uncorrected[0]][1]] for t_uncorrected in ts_uncorrected]
