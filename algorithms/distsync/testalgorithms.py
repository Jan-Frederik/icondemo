'''
Created on Jan 27, 2017

@author: mb @ pozyx labs
'''

from distsync.geometry import Geometry
from distsync.synchronizer import Synchronizer
import numpy as np
#import matplotlib.pyplot as plt

if __name__ == '__main__':
    n_tests = 100
    tot_mean_std = 0
    for i in range(n_tests):
        ### DEFINE ANCHORS AND LOCATIONS ###
        #anchor_locs = [(i*4000,j*4000,2000) for i in range(2) for j in range(2)]
        anchor_locs = [(2000,2000,2000),(10000,2000,2001),(2000,7500,2002),(10000,7500,2003)]
        g = Geometry(anchor_locs)
        n_a = len(g.anchor_list)
        
        ### DEFINE SYNCHRONIZER ###
        meas_distances = np.zeros((n_a,n_a))
        for a1i in range(n_a):
            a1 = g.anchor_list[a1i]
            for a2i in range(n_a):
                a2 = g.anchor_list[a2i]
                meas_distances[a1i,a2i] = a1.twr_range(a2)
        s = Synchronizer(n_a, meas_distances)
        
        ### DEFINE CONSTANTS FOR BEACON SIMULATION ###
        real_distances = np.zeros((n_a,n_a))
        for a1i in range(n_a):
            a1 = g.anchor_list[a1i]
            for a2i in range(n_a):
                a2 = g.anchor_list[a2i]
                real_distances[a1i,a2i] = a1.get_range(a2)
                
        ### START RUN ###
        t_s = []
        for t in np.arange(10000*3e11,10100*3e11,3e11):
            # Evaluate before #
            t_list = []
            for ai in range(n_a):
                a = g.anchor_list[ai]
                t_list.append(s.correct_time(ai,a.get_clock_time(t)))
            t_s.append(t_list)
            
            # Simulate beacons #
            beacons = []
            delta_t = 1e-3*3e11
            for a1i in range(n_a):
                a1 = g.anchor_list[a1i]
                for a2i in range(n_a):
                    a2 = g.anchor_list[a2i]
                    if (not a1i==a2i) and real_distances[a1i,a2i]<(12000+np.random.normal(0,1)):
                        a1t = a1.get_clock_time(t)
                        a2t = a2.get_clock_time(t+real_distances[a1i,a2i])
                        beacons.append([a1i,a1t, a2i,a2t])
                break
                t += delta_t
            
            # Calculate synchronization #
            s.load_beacons(beacons)
            s.forget_beacons(forget_g=10)
            s.synchronize(True)
            
            # Evaluate after #
            t_list = []
            for ai in range(n_a):
                a = g.anchor_list[ai]
                t_list.append(s.correct_time(ai,a.get_clock_time(t)))
            t_s.append(t_list)
            
        tot_mean_std += np.mean([np.std(t) for t in t_s[10:]])
    print(tot_mean_std/100)
    