'''
Created on Feb 9, 2017

@author: mb @ pozyx labs
'''

import numpy as np
from scipy import signal

class FIR:

    def __init__(self, n=5, f=0.1):
        self.n = n
        self.taps = signal.firwin(n, f)
        self.last_samples = []
        
    def add_sample(self, new_sample):
        while len(self.last_samples)<=self.n:
            self.last_samples.append(new_sample)
        while len(self.last_samples)>self.n:
            del self.last_samples[0]
        
    def filter(self, new_sample):
        self.add_sample(new_sample)
        return np.sum([self.last_samples[-1-i]*self.taps[-1-i] for i in range(min(len(self.taps),len(self.last_samples)))])
        