'''
Created on Feb 9, 2017

@author: MichielB
'''

import numpy as np

class MovingAverage:

    def __init__(self, n=5):
        self.n = n
        self.last_samples = []
        
    def add_sample(self, new_sample):
        self.last_samples.append(new_sample)
        while len(self.last_samples)>self.n:
            del self.last_samples[0]
        
    def filter(self, new_sample):
        self.add_sample(new_sample)
        return np.mean(self.last_samples)
        