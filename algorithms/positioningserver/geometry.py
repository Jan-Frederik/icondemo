'''
Created on Feb 1, 2017

@author: mb @ pozyx labs
'''

from distsync.anchor import Anchor
from totalsim.tag import Tag

from numpy.random import random

class Geometry:

    def __init__(self, space = ((0,12000),(0,9500),(0,2000)), anchor_locs = [(2000,2000,2000),(10000,2000,2001),(2000,7500,2002),(10000,7500,2003)], tag_locs=[(6000,4750,2000)]):
        self.space = space
        self.set_anchors(anchor_locs)
        self.tag_list = []
        for tag_loc in tag_locs:
            tag = Tag(tag_loc)
            tag.set_clock_random()
            self.tag_list.append(tag)
            
    def set_anchors(self, anchor_locs):
        self.anchor_list = []
        for anchor_loc in anchor_locs:
            anchor = Anchor(anchor_loc)
            anchor.set_clock_random()
            self.anchor_list.append(anchor)
        
    def get_random_point(self):
        return (random()*self.diag[0], random()*self.diag[1], random()*self.diag[2])
    
    def set_tags_random(self):
        for t in self.tag_list:
            t.loc = self.get_random_point()
            
    def set_tags(self, tag_locs):
        if len(tag_locs)==len(self.tag_list):
            for ti in range(len(self.tag_list)):
                self.tag_list[ti].loc = tag_locs[ti]