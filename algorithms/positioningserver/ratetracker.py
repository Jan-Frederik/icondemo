'''
Created on Feb 10, 2017

@author: mb @ pozyx labs
'''

import time

class RateTracker:

    def __init__(self, delta_t=0.5):
        self.delta_t = delta_t
        self.bin_start_t = time.time()
        self.count = 0
        self.rate = 0
        
    def add_count(self):
        if time.time()<(self.bin_start_t+self.delta_t):
            self.count += 1
        else:
            self.rate = self.count/self.delta_t
            self.count = 0
            self.bin_start_t = time.time()