'''
Created on Feb 1, 2017

@author: mb @ pozyx labs
'''

import numpy as np

def euclid_dist(loc1, loc2):
    return ( (loc1[0]-loc2[0])**2 + (loc1[1]-loc2[1])**2 + (loc1[2]-loc2[2])**2 )**0.5

class Tag:

    def __init__(self, loc=(0,0,0), clock=(1.000000000,0)):
        self.loc = loc
        self.clock = clock
        
    def get_range(self, anchor):
        return euclid_dist(self.loc,anchor.loc)
        
    def set_clock_random(self, std_dev=(3e-8,24*60*60*3e11)):
        self.clock = (np.random.normal(1.000000000,std_dev[0]),np.random.normal(0,std_dev[1]))
        
    def twr_range(self, anchor):
        return (self.get_range(anchor)*(self.clock[0]+anchor.clock[0])/2)