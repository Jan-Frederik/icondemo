'''
Created on Feb 2, 2017

@author: MichielB
'''

import json
import numpy as np

class TdoaFileReader:
    
    def __init__(self, options={}):
        self.options = options
        self.file_lines = []
        self.anchor_dict = {}
        
    def read_file(self, fname):
        with open(fname) as f:
            self.file_lines = f.readlines()
        self.file_lines.append('    ')
            
    def get_next_command(self):
        if len(self.file_lines)>0:
            first_line = self.file_lines[0]
            if '### ANCHOR POSITIONS ###' in first_line: # anchor positions
                del self.file_lines[0]
                anchor_i = 0
                anchor_coords = []
                while(True):
                    coord_list = self.file_lines[0][:-1].split(' | ')
                    if not len(coord_list)==4:
                        break
                    if not coord_list[0] in self.anchor_dict:   # new anchor: add to dict, increase anchor_i and add to anchor_coords
                        self.anchor_dict[coord_list[0]] = anchor_i
                        anchor_i += 1
                        anchor_coords.append([int(c) for c in coord_list[1:]])
                    del self.file_lines[0]
                return ['aps',anchor_coords]
            elif '### ANCHOR TWRS ###' in first_line: # anchor two way rangings
                del self.file_lines[0]
                anchor_twrs = []
                while True:
                    anchors = self.file_lines[0][:-1].split(' | ')
                    if not len(anchors)==2 or not anchors[0] in self.anchor_dict or not anchors[1] in self.anchor_dict:
                        break
                    del self.file_lines[0]
                    tofs = []
                    while True:
                        tof = self.file_lines[0][:-1].split(':  ')
                        if not tof[0]=='time of flight':
                            break
                        del self.file_lines[0]
                        tofs.append(float(tof[1]))
                    tof = np.mean(tofs)*3e11
                    anchor_twrs.append((self.anchor_dict[anchors[0]],self.anchor_dict[anchors[1]],tof))
                return ['ars',anchor_twrs]
            elif 'Sync, Master' in first_line: # sync
                send_id = int(self.anchor_dict[(first_line[:-1].split(', ')[1]).split(':')[1]])
                send_timestamp = int((first_line[:-1].split(', ')[2]).split(':')[1])
                del self.file_lines[0]
                sync_sigs = []
                while True:
                    sync = self.file_lines[0][:-1].split(', ')
                    if sync[1].split(':')[1] in self.anchor_dict:
                        if not 'Sync, Rx_Anchor' in self.file_lines[0]:
                            break
                        receive_id = int(self.anchor_dict[(sync[1]).split(':')[1]])
                        receive_timestamp = int((sync[3]).split(':')[1])
                        sync_sigs.append([send_id,send_timestamp, receive_id,receive_timestamp])#*3e11/(128*499.2e6)#*3e11/(128*499.2e6)
                    else:
                        break
                    del self.file_lines[0]
                return ['snc',sync_sigs]
            elif 'Blink' in first_line: # blink
                blink_id = int((first_line[:-1].split(', ')[4]).split(':')[1])
                tag_id = (first_line[:-1].split(', ')[2]).split(':')[1]
                blink_sigs = []
                while True:
                    blink = self.file_lines[0][:-1].split(', ')
                    if not 'Blink' in self.file_lines[0] or not int(blink[4].split(':')[1])==blink_id or not blink[2].split(':')[1]==tag_id:
                        break
                    #print(self.file_lines[0][:-1])
                    if blink[1].split(':')[1] in self.anchor_dict:
                        blink_sigs.append([int(self.anchor_dict[blink[1].split(':')[1]]), int((blink[3].split(':'))[1])])#*3e11/(128*499.2e6)
                    del self.file_lines[0]
                #print([tag_id,blink_sigs])
                return ['blk',[tag_id,blink_sigs]]
            else: # this line is rubbish, remove and continue
                del self.file_lines[0]
                return self.get_next_command()
        return ['eof',None]
    
    
class TdoaJsonReader:
    
    def __init__(self, options={}):
        self.options = options
        self.json_object = None
        self.anchor_dict = {}
        self.sorted_i_list = []
        
    def read_file(self, fname):
        with open(fname) as f:
            self.json_object = json.load(f)
        i_list = [int(i) for i in self.json_object]
        self.sorted_i_list = [str(i) for i in np.sort(i_list)]
            
    def get_next_command(self):
        packet_dict = dict(self.json_object[self.sorted_i_list.pop(0)])
        return packet_dict
        
if __name__ == '__main__':
    tjr = TdoaJsonReader()
    tjr.read_file('C:\\Users\\MichielB\\Desktop\\example_packets.json')
    tjr.get_next_command()
    tjr.get_next_command()
    tjr.get_next_command()
    tjr.get_next_command()
    tjr.get_next_command()