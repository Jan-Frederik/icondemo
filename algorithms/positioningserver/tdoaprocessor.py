'''
Created on Feb 9, 2017

@author: mb @ pozyx labs
'''

import socket
import struct

import numpy as np

from algorithms.distsync.synchronizer import Synchronizer
from algorithms.tdoaloc.locatormin import LocatorMin
from algorithms.positioningserver.ratetracker import RateTracker
from algorithms.filter.fir import FIR

ANCHOR_LOCS = [[9954-720, 50, 2377], [9954-9954, -554, 2381], [9954-9620, 6581, 2379], [9954-681, 6581, 2360]]

ANCHOR_DISTS = np.array([[0., 9455.75, 11230.47, 6934.53],
                         [9455.75, 0., 0., 0.],
                         [11230.47, 0., 0., 0.],
                         [6934.53, 0., 0., 0.]])


ANCHOR_IDS_IN = {0xA001: 0, 0xA002: 1, 0xA003: 2, 0xA004: 3}
ANCHOR_IDS_OUT = {0: 0xA001, 1: 0xA002, 2: 0xA003, 3: 0xA004}

def calc_rss(max_growth_cir, rx_pream_count):
    #float A[3] = {0.0f, 115.72f, 121.74f};      // A is a constant for PRF 16MHz and 64MHz, first value is a dummy placeholder
    #float val = ((float)diag->maxGrowthCIR) / diag->rxPreamCount / diag->rxPreamCount;
    #float rx_dbm = 10.0f * log10(val) + 51.1751f - A[2];    // 10log10(2^17) = 51.1751
    return 10.0*np.log10(max_growth_cir/rx_pream_count**2)+51.1751-121.74

def set_sync_period(sync_period):
    value = int(249600000 * sync_period) #second = 249600000
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    data = struct.pack("BBHI",1,7, 26,value)
    s.sendto(data, ("192.168.100.1",7777))

def set_number_of_tags(number_of_tags):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    data = struct.pack("BBHB", 1, 7, 38, number_of_tags)
    s.sendto(data, ("192.168.100.1", 7777))

class RxAnchorTimeAggregator:

    def __init__(self, n_samples):
        self.n_samples = n_samples
        self.sample_list = []
        self.sample_agg = []

    def update(self, rx_anchors):
        self.sample_list.append(rx_anchors)
        if len(self.sample_list)==self.n_samples: # if list full: aggregate rx_anchor_times and return True
            rx_anchors_agg = {}
            for sample in self.sample_list:
                for anchor in sample:
                    if anchor[0] in rx_anchors_agg:
                        rx_anchors_agg[anchor[0]] = [rx_anchors_agg[anchor[0]][0]+anchor[1],rx_anchors_agg[anchor[0]][1]+1]
                    else:
                        rx_anchors_agg[anchor[0]] = [anchor[1],1]
            #self.sample_agg = rx_anchors
            self.sample_agg = [[rx_anchor,rx_anchors_agg[rx_anchor][0]/self.n_samples] for rx_anchor in rx_anchors_agg if rx_anchors_agg[rx_anchor][1]==self.n_samples]
            self.sample_list = []
            return True
        return False

class TagInfo:
    filter_n_samples = 20
    filter_f_cutoff = 0.1

    def __init__(self, anchor_locations = ANCHOR_LOCS):
        self.l = LocatorMin(anchor_locations)
        self.f = [FIR(type(self).filter_n_samples, type(self).filter_f_cutoff) for i in range(3)]
        self.update_rate = RateTracker(1.0)
        self.packet_loss = 0
        self.rx_anchor_aggregator = RxAnchorTimeAggregator(1)
        self.position = []

    def switch_off_filter(self):
        self.f = []

    def switch_on_filter(self, n, f):
        self.f = [FIR(n,f) for i in range(3)]

    def update_filter(self):
        self.f = [FIR(type(self).filter_n_samples, type(self).filter_f_cutoff) for i in range(3)]

    def update(self, rx_anchors_sync):
        if self.rx_anchor_aggregator.update(rx_anchors_sync): # if rx_anchor_aggregator produces new output: calculate and filter position and return True
            self.position = self.l.get_tag_loc(self.rx_anchor_aggregator.sample_agg)
            if len(self.f)>0:
                self.position = [self.f[i].filter(self.position[i]) for i in range(3)]
            self.update_rate.add_count() # update update rate
            return True
        return False

class TdoaProcessor:

    def __init__(self, sync_input_queue, sync_output_queue, blink_input_queue, blink_output_queue, settings_queue, max_number_of_tags=10, sync_period = 0.1, anchor_locations = ANCHOR_LOCS, anchor_distances = ANCHOR_DISTS, anchor_ids_in = ANCHOR_IDS_IN, anchor_ids_out = ANCHOR_IDS_OUT):
        self.sync_input_queue = sync_input_queue
        self.sync_output_queue = sync_output_queue
        self.blink_input_queue = blink_input_queue
        self.blink_output_queue = blink_output_queue
        self.settings_queue = settings_queue

        self.anchor_ids_in = anchor_ids_in
        self.anchor_ids_out = anchor_ids_out
        self.anchor_locations = anchor_locations

        self.sync_period = sync_period
        set_sync_period(sync_period)
        self.max_number_of_tags = max_number_of_tags
        set_number_of_tags(max_number_of_tags)

        self.s = Synchronizer(anchor_distances)
        self.t = {}

    def start(self):
        old_blink_index = -1
        while True:
            if not self.sync_input_queue.empty():
                sync_input = self.sync_input_queue.get()
                #print(sync_input)
                # CONVERT FROM INPUT
                beacons = []
                tx_id = sync_input['tx_id']
                tx_ts = sync_input['tx_timestamp']
                for rx_anchor in sync_input['rx_data']:
                    rx_id = rx_anchor['rx_id']
                    rx_ts = rx_anchor['timestamp']
                    beacons.append([self.anchor_ids_in[tx_id], tx_ts, self.anchor_ids_in[rx_id], rx_ts])
                # PROCESS
                self.s.calc_sync(beacons)
                # CONVERT TO OUTPUT
                #print(self.s.desync[0,:])
                sync_output = {self.anchor_ids_out[ai]:self.s.desync[0,ai] for ai in self.anchor_ids_out}
                self.sync_output_queue.put(sync_output)
            elif not self.blink_input_queue.empty():
                blink_input = self.blink_input_queue.get()
                while not self.blink_input_queue.empty(): # get the last position blink update
                    blink_input = self.blink_input_queue.get()
                # print(blink_input)
                # CONVERT FROM INPUT
                rx_anchors = [[self.anchor_ids_in[rx_anchor['rx_id']], rx_anchor['timestamp']] for rx_anchor in blink_input['rx_data']]
                # PROCESS
                tag_id = blink_input['tx_id']
                if not tag_id in self.t:                                    # update tag dict if new tag
                    self.t[tag_id] = TagInfo(self.anchor_locations)
                tag_info = self.t[tag_id]
                if old_blink_index>=0 and blink_input['index']>(old_blink_index+1): # update packet loss for tag
                    tag_info.packet_loss += blink_input['index']-old_blink_index-1
                if blink_input['index']<old_blink_index:
                    tag_info.packet_loss = 0
                old_blink_index = blink_input['index']
                if tag_info.update( self.s.correct_times(rx_anchors) ):     # if update produces new output data put on queue
                    # calculate rss for different anchors
                    rx_rss = {self.anchor_ids_out[self.anchor_ids_in[rx_anchor['rx_id']]]:calc_rss(rx_anchor['diagnostics'][5],rx_anchor['diagnostics'][6]) for rx_anchor in blink_input['rx_data'] if len(rx_anchor['diagnostics'])>0}
                    # CONVERT TO OUTPUT
                    blink_output = {'tag_id': blink_input['tx_id'],
                                    'x': tag_info.position[0], 'y': tag_info.position[1], 'z': tag_info.position[2], 'a_x': blink_input['extras'][0], 'a_y': blink_input['extras'][1], 'a_z': blink_input['extras'][2],
                                    'sigma': 0, 'update_rate': tag_info.update_rate.rate, 'packet_loss': tag_info.packet_loss/self.sync_period,
                                    'orientation': {'Pitch': blink_input['extras'][5]/16., 'Yaw': blink_input['extras'][3]/16.}, 'rss': rx_rss,
                                    'anchor_times': {self.anchor_ids_out[rx_anchor[0]]:rx_anchor[1] for rx_anchor in tag_info.rx_anchor_aggregator.sample_agg}}
                    self.blink_output_queue.put(blink_output)
            if not self.settings_queue.empty():
                new_settings = self.settings_queue.get()
                for new_setting in new_settings:
                    if new_setting == 'Number of samples':
                        TagInfo.filter_n_samples = new_settings[new_setting]
                        for ti in self.t:
                            self.t[ti].update_filter()
                    elif new_setting == 'Cut-off':
                        TagInfo.filter_f_cutoff = new_settings[new_setting]
                        for ti in self.t:
                            self.t[ti].update_filter()
                    elif new_setting == 'Number of tags':
                        set_number_of_tags(new_settings[new_setting])
                    elif new_setting == 'Sync period':
                        set_sync_period(new_settings[new_setting])
