'''
Created on Feb 2, 2017

@author: mb @ pozyx labs
'''

from positioningserver.tdoafilereader import TdoaJsonReader
from positioningserver.tdoaprocessor import TdoaProcessor

from multiprocessing import Process, Queue
import time

def print_out(osq,obq):
    while True:
        if not osq.empty():
            print(osq.get())
        elif not obq.empty():
            print(obq.get())
        
def load_in(isq,ibq):
    tjr = TdoaJsonReader()
    tjr.read_file('C:\\Users\\MichielB\\Desktop\\example_packets.json')
    '''read_res = ['bof',None]
    while(not read_res[0]=='eof'):
        read_res = tdoafr.get_next_command()
        if read_res[0]=='snc':
            data = {'sync_id': 0, 'tx_anchor': read_res[1][0][0], 'tx_timestamp': read_res[1][0][1], 'rx_anchors': [{'rx_anchor': rr[2], 'rx_timestamp': rr[3]} for rr in read_res[1]]}
            print(data)
            isq.put(data)
        elif read_res[0]=='blk':
            data = {'blink_id': 0, 'tag_id':read_res[1][0], 'rx_anchors': [{'rx_anchor': rr[0], 'rx_timestamp': rr[1]} for rr in read_res[1][1]]}
            print(data)
            ibq.put(data)'''
    while(len(tjr.sorted_i_list)>0):
        data = tjr.get_next_command()
        if 'tx_timestamp' in data:
            isq.put(data)
        else:
            ibq.put(data)
        time.sleep(0.0025)

if __name__ == '__main__':
    in_sync_queue = Queue()
    in_blink_queue = Queue()
    out_sync_queue = Queue()
    out_blink_queue = Queue()
    
    p1 = Process(target=load_in, args=(in_sync_queue, in_blink_queue))
    
    tp = TdoaProcessor(in_sync_queue, out_sync_queue, in_blink_queue, out_blink_queue)
    p2 = Process(target=tp.start)
    
    p3 = Process(target=print_out, args=(out_sync_queue, out_blink_queue))
    
    p3.start()
    print('out printer started')
    p2.start()
    print('processor started')
    p1.start()
    print('in reader started')
    
    '''
    colors = ['r','b','g','m','c','y','k']
    plt.figure()
    for i in range(len(loc_timeseries)):
        xs = np.array([ls[0] for ls in loc_timeseries[i]])
        ys = np.array([ls[1] for ls in loc_timeseries[i]])
        plt.scatter(xs, ys, s=2, color=colors[i%7],label=str(i))
    plt.legend()
    plt.xlim([-1000,8369+1000])
    plt.ylim([-1000,5614+1000])
    plt.show()
    '''