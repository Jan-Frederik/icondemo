'''
Created on Feb 2, 2017

@author: mb @ pozyx labs
'''

import numpy as np
from positioningserver.tdoafilereader import TdoaFileReader
#from positioningserver.geometry import Geometry
from distsync.synchronizer import Synchronizer
from tdoaloc.locatormin import LocatorMin
import matplotlib.pyplot as plt
import time

#from filter.movingaverage import MovingAverage
#from filter.movingmedian import MovingMedian
from filter.fir import FIR

if __name__ == '__main__':
    tdoafr = TdoaFileReader()
    tdoafr.read_file('C:\\Users\\MichielB\\Desktop\\dataset4\\4tags.txt')

    loc_timeseries = []
    loc_secondseries = []
    
    num_syncs = 0
    num_anchors = 0
    
    strt_s=0
    
    read_res = ['bof',None]
    while(not read_res[0]=='eof'):
        read_res = tdoafr.get_next_command()
        #print(read_res)
        if read_res[0]=='aps':
            #g = Geometry([[min([ap[i] for ap in read_res[1]]), max([ap[i] for ap in read_res[1]])] for i in range(3)] , read_res[1] , [])
            li = read_res[1]
            print(li)
            l = {}
            f = {}
            num_anchors = len(read_res[1])
            anchor_times_old = {}
        elif read_res[0]=='ars':
            meas_dists = np.zeros((num_anchors,num_anchors))
            for md in read_res[1]:
                meas_dists[md[0],md[1]] = md[2]
                meas_dists[md[1],md[0]] = md[2]
            print(meas_dists)
            s = Synchronizer(meas_dists)
        elif read_res[0]=='snc':
            if not strt_s==0:
                print('BLINK S: '+str(time.time()-strt_s)+'s')
            strt = time.time()
            s.load_beacons(read_res[1])
            s.forget_beacons(forget_g=5)
            s.synchronize(rate_bool=True)
            print(s.desync[0,:])
            loc_timeseries.append(loc_secondseries)
            loc_secondseries = []
            num_syncs += 1
            print('SYNC: '+str(time.time()-strt)+'s')
            strt_s = time.time()
        elif read_res[0]=='blk':
            if len(read_res[1][1])==num_anchors:
                #print(read_res[1][1])
                anchor_times = s.correct_times(read_res[1][1])
                
                if not read_res[1][0] in anchor_times_old:
                    anchor_times_old[read_res[1][0]] = anchor_times
                else:
                    m_anchor_times = []
                    for i in range(num_anchors):
                        for at in anchor_times:
                            if at[0]==i:
                                for ato in anchor_times_old[read_res[1][0]]:
                                    if ato[0]==i:
                                        m_anchor_times.append([i,(at[1]+ato[1])/2])
                                        break
                            break
                        
                    del anchor_times_old[read_res[1][0]]
                                
                    #print([[a[0],a[1]-np.mean([aa[1] for aa in anchor_times])] for a in anchor_times])
                    if num_syncs>5:
                        #print([[i[0], i[1]-np.mean([j[1] for j in anchor_times])] for i in anchor_times])
                        if not read_res[1][0] in l:
                            l[read_res[1][0]] = LocatorMin(li)
                            f[read_res[1][0]] = [FIR(100,0.005) for i in range(3)]#[MovingMedian(10) for i in range(3)]#[MovingAverage(10) for i in range(3)]
                        position = l[read_res[1][0]].get_tag_loc(anchor_times)
                        loc_secondseries.append([position[i] for i in range(3)])#f[read_res[1][0]][i].filter()
                        #print(read_res[1][0])
                        #print(loc_secondseries[-1])
            else:
                print('Not all anchors received blink.')
    
    
    colors = ['r','b','g','m','c','y','k']
    plt.figure()
    for i in range(len(loc_timeseries)):
        xs = np.array([ls[0] for ls in loc_timeseries[i]])
        ys = np.array([ls[1] for ls in loc_timeseries[i]])
        plt.scatter(xs, ys, s=2, color=colors[i%7],label=str(i))
    plt.legend()
    plt.xlim([-1000,8369+1000])
    plt.ylim([-1000,5614+1000])
    plt.show()