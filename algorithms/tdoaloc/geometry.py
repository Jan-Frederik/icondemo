'''
Created on Jan 5, 2017

@author: mb @ pozyx labs
'''
from numpy.random import random

class Geometry:
    
    def __init__(self, diag = (12000,9500,2000), anchor_positions = [(2000,2000,2000),(10000,2000,2001),(2000,7500,2002),(10000,7500,2003)]):
        self.diag = diag
        self.anchor_positions = anchor_positions
        
    def set_anchors(self, anchor_positions):
        self.anchor_positions = anchor_positions
        
    def get_random_point(self):
        return (random()*self.diag[0], random()*self.diag[1], random()*self.diag[2])
    
if __name__ == '__main__':
    g = Geometry()
    for i in range(100):
        print(g.get_random_point())