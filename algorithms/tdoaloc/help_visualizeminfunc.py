'''
Created on Jan 20, 2017

@author: mb @ pozyx labs
'''

from tdoaloc.geometry import Geometry
from regression.relevancevectormachine import RelevanceVectorMachine

import numpy as np
import matplotlib.pyplot as plt

def euclid_dist(loc1, loc2):
    return ( (loc1[0]-loc2[0])**2 + (loc1[1]-loc2[1])**2 + (loc1[2]-loc2[2])**2 )**(0.5)

def get_anchor_time(anchor_loc, tag_loc):
    dist = euclid_dist(anchor_loc, tag_loc)
    return dist+np.random.normal(0, 250)#()/3e11 #in s#

def get_anchor_times(anchor_locs, tag_loc):
    return [get_anchor_time(anchor_loc, tag_loc) for anchor_loc in anchor_locs]

def heatmap_rvm(xs, ys, x_evals, vrange=[0,10000]): 
    #train
    rvm = RelevanceVectorMachine([1/2/4/250**2, 1/2/4/500**2, 1/2/4/1000**2])
    rvm.fit(np.array(xs), np.array(ys))
    #evaluate and plot on fixed raster
    fig = plt.figure(figsize=(20, 12))
    for t_i in range(len(x_evals[3])):
        t_eval = x_evals[3][t_i]
        for z_i in range(len(x_evals[2])):
            z_eval = x_evals[2][z_i]
            ax = fig.add_subplot(len(x_evals[2]), len(x_evals[3]), z_i*len(x_evals[3])+t_i+1)
            heatmap = [ [ rvm.predict(np.array([[x_eval,y_eval,z_eval,t_eval]]), '')[0][0] for x_eval in x_evals[0]] for y_eval in x_evals[1]]
            ax.imshow(heatmap, interpolation='nearest', vmin=vrange[0], vmax=vrange[1])
            
def heatmap_exact(g, anchor_times, x_evals, vrange=[0,10000]):
    #evaluate and plot on fixed raster
    fig = plt.figure(figsize=(20, 12))
    for t_i in range(len(x_evals[3])):
        t_eval = x_evals[3][t_i]
        for z_i in range(len(x_evals[2])):
            z_eval = x_evals[2][z_i]
            ax = fig.add_subplot(len(x_evals[2]), len(x_evals[3]), z_i*len(x_evals[3])+t_i+1)
            heatmap = [ [ (sum([( (x_eval-g.anchor_positions[i][0])**2 + (y_eval-g.anchor_positions[i][1])**2 + (z_eval-g.anchor_positions[i][2])**2 - (t_eval-anchor_times[i])**2 )**2 for i in range(len(g.anchor_positions))]))**0.25 for x_eval in x_evals[0]] for y_eval in x_evals[1]]
            ax.imshow(heatmap, interpolation='nearest', vmin=vrange[0], vmax=vrange[1])
            
    

if __name__ == '__main__':
    g = Geometry()
    #tag_loc = g.get_random_point()
    tag_loc = (4100, 4100, 3900)
    print(tag_loc)
    anchor_times = get_anchor_times(g.anchor_positions, tag_loc)
    
    '''n_tests = 1000
    xs = []
    ys = []
    for i in range(n_tests):
        x_test = g.get_random_point()
        t_test = min(anchor_times)-np.random.random()*6000
        minimize_function = (sum([( (x_test[0]-g.anchor_positions[i][0])**2 + (x_test[1]-g.anchor_positions[i][1])**2 + (x_test[2]-g.anchor_positions[i][2])**2 - (t_test-anchor_times[i])**2 )**2 for i in range(len(g.anchor_positions))]))**0.25
        xs.append([x_test[0] , x_test[1] , x_test[2] , t_test])
        ys.append([minimize_function])'''
        
    d_sample = 250
    x_evals = range(0,g.diag[0]+1,d_sample)
    y_evals = range(0,g.diag[1]+1,d_sample)
    z_evals = range(0,g.diag[2]+1,2*d_sample)
    #x_evals = range(int(tag_loc[0]-10*d_sample),int(tag_loc[0]+10*d_sample+1),d_sample)
    #y_evals = range(int(tag_loc[1]-10*d_sample),int(tag_loc[1]+10*d_sample+1),d_sample)
    #z_evals = range(int(tag_loc[2]-10*d_sample),int(tag_loc[2]+10*d_sample+1),d_sample)
    t_evals = range((int(max(anchor_times)/d_sample)+1)*d_sample,-(int(max(anchor_times)/d_sample)+1)*d_sample-1,-5*d_sample)
    print(list(x_evals))
    print(list(y_evals))
    print(list(z_evals))
    print(list(t_evals))
    #heatmap_rvm(xs, ys, [x_evals, y_evals, z_evals, t_evals])
    heatmap_exact(g, anchor_times, [x_evals, y_evals, z_evals, t_evals])
    plt.show()