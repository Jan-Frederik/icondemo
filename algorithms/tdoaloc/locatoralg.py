'''
Created on Jan 5, 2017

@author: mb @ pozyx labs
'''

import numpy as np

class LocatorAlg:
    
    def __init__(self, anchor_positions):
        self.anchor_positions = anchor_positions
        
    def get_tag_loc(self, anchor_times):
        g = np.array( [ [ self.anchor_positions[i][0]-self.anchor_positions[0][0] ,
                          self.anchor_positions[i][1]-self.anchor_positions[0][1] , 
                          #self.anchor_positions[i][2]-self.anchor_positions[0][2] ,
                          (anchor_times[i]-anchor_times[0]) ] 
                        for i in range(1,len(anchor_times)) ] )
        h = np.array( [ [ (self.anchor_positions[i][0]-self.anchor_positions[0][0])**2
                          + (self.anchor_positions[i][1]-self.anchor_positions[0][1])**2
                          #+ (self.anchor_positions[i][2]-self.anchor_positions[0][2])**2
                          - ((anchor_times[i]-anchor_times[0]))**2 ]
                        for i in range(1,len(anchor_times)) ] ) * 0.5
        gtg = np.dot( np.transpose(g) , g )
        gtg_inv = np.linalg.inv( gtg )
        gth = np.dot( np.transpose(g) , h )
        eta_est = np.dot( gtg_inv , gth )
        return ( eta_est[0][0]+self.anchor_positions[0][0] , eta_est[1][0]+self.anchor_positions[0][1] , self.anchor_positions[0][2] )#eta_est[2][0]+