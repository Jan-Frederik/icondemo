'''
Created on Jan 23, 2017

@author: mb @ pozyx labs
'''

import numpy as np
from scipy.optimize import minimize

class LocatorMin:

    def __init__(self, anchor_positions, tag_position=None):
        self.anchor_positions = anchor_positions
        self.tag_position = tag_position
        if tag_position is None:
            self.tag_position = [np.mean([p[i] for p in self.anchor_positions]) for i in range(3)]

    def get_tag_loc(self, anchor_times, tag_position=None):
        if len(anchor_times)<4:
            return self.tag_position
        loc_anchors = [[self.anchor_positions[at[0]][0],self.anchor_positions[at[0]][1],self.anchor_positions[at[0]][2],at[1]-np.mean([a[1] for a in anchor_times])] for at in anchor_times]
        #anchor_ids = np.array(anchor_times).argsort()[-7:]
        #anchor_ids = [ai for ai in range(len(anchor_times)) if anchor_times[ai]>1000]

        def loc_func(x):
            func_val = sum([( ((x[0]-l[0])**2 + (x[1]-l[1])**2) - np.abs(x[3]-l[3])**2 )**2 for l in loc_anchors])# + (x[2]-l[2])**2
            #func_val = sum([np.abs( ((x[0]-l[0])**2 + (x[1]-l[1])**2)**0.5 - np.abs(x[3]-l[3])**1 )**1 for l in loc_anchors])# + (x[2]-l[2])**2
            return func_val

        def loc_jac(x):
            func_val = 4*sum( [ np.array([x[0]-l[0],x[1]-l[1],0,-(x[3]-l[3])])*( ((x[0]-l[0])**2 + (x[1]-l[1])**2) - np.abs(x[3]-l[3])**2 ) for l in loc_anchors ] )
            return func_val

        def loc_hess(x):
            return 0

        #x0 = self.la.get_tag_loc(anchor_times)
        x0 = tag_position
        if x0 is None:
            x0 = self.tag_position
            #x0 = [np.mean([p[i] for p in self.anchor_positions]) for i in range(3)]
        t_temp = loc_anchors[0][3]-((x0[0]-loc_anchors[0][0])**2+(x0[1]-loc_anchors[0][1])**2+(x0[2]-loc_anchors[0][2])**2)**0.5
        res = minimize(loc_func, np.array([x0[0],x0[1],x0[2],t_temp]), method='BFGS', jac=loc_jac, options={'disp': False, 'maxiter': 15, 'gtol': 1e08})#'xtol': 100,
        #print(res.x[3])
        loc = [res.x[i] for i in range(3)]
        if np.abs(loc[0])>50000 or np.abs(loc[1])>50000: # DIRTY HACK TO PREVENT RIDICULOUS POSITIONS
            return self.tag_position
        self.tag_position = loc
        return loc
