'''
Created on Jan 5, 2017

@author: mb @ pozyx labs
'''

from tdoaloc.geometry import Geometry
from tdoaloc.locatoralg import LocatorAlg
from tdoaloc.locatormin import LocatorMin
import matplotlib.pyplot as plt

import numpy as np
from tdoaloc.help_visualizeminfunc import heatmap_rvm

def euclid_dist(loc1, loc2):
    return ( (loc1[0]-loc2[0])**2 + (loc1[1]-loc2[1])**2 )**(0.5)# + (loc1[2]-loc2[2])**2

def get_anchor_time(anchor_loc, tag_loc):
    dist = euclid_dist(anchor_loc, tag_loc)
    return dist+np.random.normal(0, 60)

def get_anchor_times(anchor_locs, tag_loc):
    return [get_anchor_time(anchor_loc, tag_loc) for anchor_loc in anchor_locs]

if __name__ == '__main__':
    g = Geometry()
    la = LocatorAlg(g.anchor_positions)
    lm = LocatorMin(g.anchor_positions)
    n_tests = 2000
    test_locs = []
    test_results_la = []
    test_results_lm = []
    for i in range(n_tests):
        tag_loc = g.get_random_point()
        anchor_times = get_anchor_times(g.anchor_positions, tag_loc)
        test_locs.append([tag_loc[0],tag_loc[1],tag_loc[2],0])
        #test_results_la.append([euclid_dist(tag_loc, la.get_tag_loc(anchor_times) )])
        test_results_lm.append([euclid_dist(tag_loc, lm.get_tag_loc(anchor_times) )])#, tag_loc
        #print(tag_loc)
        #print(la.get_tag_loc(anchor_times))
        #print(lm.get_tag_loc(anchor_times))
    #print(min(min(test_results_la)))
    #print(np.mean([t[0]**2 for t in test_results_la])**0.5)
    #print(max(max(test_results_la)))
    
    print(min(min(test_results_lm)))
    print(np.mean([t[0]**2 for t in test_results_lm])**0.5)
    print(max(max(test_results_lm)))
    
    d_sample = 200
    x_evals = range(0,g.diag[0]+1,d_sample)
    y_evals = range(0,g.diag[1]+1,d_sample)
    z_evals = range(0,g.diag[2]+1,2*d_sample)
    t_evals = [0]
    #heatmap_rvm(np.array(test_locs), np.array(test_results_la), [x_evals, y_evals, z_evals, t_evals], [0, 1000])
    heatmap_rvm(np.array(test_locs), np.array(test_results_lm), [x_evals, y_evals, z_evals, t_evals], [0, 1000])
    plt.show()