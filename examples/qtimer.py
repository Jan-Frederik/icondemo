import json
import random
import sys
from time import sleep, time

from PyQt5.QtCore import *
from PyQt5.QtNetwork import *
from PyQt5.QtWidgets import *


class TimerDemo(QWidget):

    def __init__(self, timer_timeout=1000):
        super(TimerDemo, self).__init__()
        self.timer = QTimer(self)
        self.timer.setInterval(timer_timeout)
        self.timer.timeout.connect(self.onTimeOut)
        self.timer.start()
        self.i = 0

        self.initUI()

    def onTimeOut(self):
        self.i += 1
        print('Timeout #%i at time %i' % (self.i, time() * 1000))

    def initUI(self):
        self.setGeometry(300, 300, 290, 150)
        self.setWindowTitle('Logger')
        self.show()


def main():

    app = QApplication(sys.argv)
    ex = TimerDemo()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
