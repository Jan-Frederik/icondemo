"""Credits Meechee"""

import socket
import struct
import time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = ('0.0.0.0', 8888)

s.bind(server_address)

while True:
    data, address = s.recvfrom(1024)
    # timestampdata = bytes([data[0]]) + bytes([data[1]]) + bytes([data[2]]) + bytes([data[3]]) + bytes([data[4]]) + b'\x00' + b'\x00' + b'\x00'
    # rxdata = data[5:9]
    # timestamp = struct.unpack("Q", timestampdata)[0]
    # timestamp = ((0.000015650040064103 * ( timestamp % 1099511627776))/1000000)

    # print(len(data))
    offset = 0
    packet_type = struct.unpack("<B", data[offset:offset + 1])[0]
    offset += 1

    if packet_type & 0x80:
        maxNoise, firstPathAmp1, stdNoise, firstPathAmp2, firstPathAmp3, maxGrowthCIR, rxPreamCount, firstPath = struct.unpack("HHHHHHHH", data[
                                                                                                                               offset:offset + 16])
        offset += 16
        # print("diagnostics: maxNoise:{} firstPathAmp1:{} stdNoise:{} firstPathAmp2:{} firstPathAmp3:{} maxGrowthCIR:{} rxPreamCount:{} firstPath:{}".format(maxNoise, firstPathAmp1, stdNoise, firstPathAmp2, firstPathAmp3, maxGrowthCIR, rxPreamCount, firstPath))
        packet_type &= 0x7F

    if packet_type == 0:  # received packet sent from anchors
        anchor_id, timestamp = struct.unpack("<HQ", data[offset:offset + 2 + 8])
        offset += 10

        uwb_packet_type, uwb_packet_version = struct.unpack("<BB", data[offset:offset + 1 + 1])
        offset += 2
        if uwb_packet_type == ord('S'):
            send_anchor_id, period, sync_index, slot_length, nr_tags = struct.unpack(
                "<HIBQB", data[offset:offset + 2 + 4 + 1 + 8 + 1])
            offset += 7
            print(offset, len(data))
            print("Sync, Rx_Anchor:{}, Tx_Anchor:{}, Timestamp:{}, Sync_Index: {}, Slot_length: {}, Nr_tags: {}".format(
                hex(anchor_id), hex(send_anchor_id), timestamp, sync_index, slot_length, nr_tags))
            # print("uwb_packet_type:{} uwb_packet_version:{} anchor_id:{} period:{} sync:{}".format(uwb_packet_type, uwb_packet_version, hex(send_anchor_id), period, sync_index))
        elif uwb_packet_type == ord('B'):
            tag_id, blink_index = struct.unpack("<HH", data[offset:offset + 2 + 2])
            offset += 4
            acc_x, acc_y, acc_z, yaw, roll, pitch = struct.unpack("<hhhhhh", data[offset:offset + 12])
            offset += 12
            print("Blink, Rx_Anchor:{}, Tag_Id:{}, Timestamp:{}, Blink_Index:{}, Acc_x:{}, Acc_y:{}, Acc_z:{}, Yaw:{}, Roll:{}, Pitch:{}".format(
                hex(anchor_id), hex(tag_id), timestamp, blink_index, acc_x, acc_y, acc_z, yaw, roll, pitch))
            # print("uwb_packet_type:{} uwb_packet_version:{} tag_id:{} blink_index:{}".format(uwb_packet_type, uwb_packet_version, hex(tag_id), blink_index))

    elif packet_type == 1:  # sync packet sent from master
        anchor_id, timestamp = struct.unpack("<HQ", data[offset:offset + 2 + 8])
        offset += 10
        sync_index = struct.unpack("<B", data[offset:offset + 1])[0]
        offset += 1
        print("Sync, Master:{}, Timestamp:{}, Sync_Index:{}".format(hex(anchor_id), timestamp, sync_index))
