import sys
import os
import time
import random
import math
from time import sleep
import numpy as np
import scipy.ndimage as ndi

import PyQt5
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtNetwork import *
import pyqtgraph as pg
from pyqtgraph import PlotWidget, GraphicsWindow

import socket
import struct
from collections import OrderedDict
from multiprocessing import Process, Queue
import threading


class TdoaMainWindow(QMainWindow):

    def __init__(self, tag_queue, sync_queue, parameter_queue, parent=None):
        super(TdoaMainWindow, self).__init__(parent)
        self.tag_queue = tag_queue
        self.sync_queue = sync_queue
        self.parameter_queue = parameter_queue
        self.update_period_gui = 0.1
        self.current_parameter_dict = {
            "Number of samples": 20, "Cut-off": 0.1, "Number of tags": 10, "Sync period": 0.2}

        self.nb_of_datapoints = 100
        self.anchor_colorset = [[0, 0, 255], [0, 255, 0], [255, 204, 0], [0, 204, 255],  [204, 0, 204],
                                [204, 255, 0], [0, 204, 204], [102, 204, 0], [0, 204, 102], [204, 102, 0], [0, 102, 204]]
        self.anchor_dict = {0xA001: Anchor(0xA001, [9954 - 720, 50], self.nb_of_datapoints, self.anchor_colorset[0], 270), 0xA002: Anchor(0xA002, [9954 - 9954, -554], self.nb_of_datapoints, self.anchor_colorset[1], 0), 0xA003: Anchor(
            0xA003, [9954 - 9620, 6581], self.nb_of_datapoints, self.anchor_colorset[2], 90), 0xA004: Anchor(0xA003, [9954 - 681, 6581], self.nb_of_datapoints, self.anchor_colorset[3], 180)}

        self.product_dict = {'Omer': Product('Omer', [2000, 5000],[200, 3500, 2600, 6000])}

        self.tag_dict = {}
        self.tag_colorset = [[0, 0, 255], [0, 255, 0], [255, 204, 0], [0, 204, 255],  [204, 0, 204],
                             [204, 255, 0], [0, 204, 204], [102, 204, 0], [0, 204, 102], [204, 102, 0], [0, 102, 204]]
        self.tag_color_index = 0
        self.heatmap = HeatMap(0, 0, 9954, 6581, 250, 99, 65, 10000)

        self.tabbing_window = QTabWidget(self)
        self.setCentralWidget(self.tabbing_window)
        self.positioning_window = PositioningWindow(self)
        self.diagnostics_window = DiagnosticsWindow(self)

        self.tabbing_window.addTab(self.positioning_window, "Positioning")
        self.tabbing_window.addTab(self.diagnostics_window, "Diagnostics")

        self.setWindowTitle("TDOA visualization")
        self.showMaximized()
        self.show()

        self.gui_update_timer = QTimer(self)
        self.gui_update_timer.setInterval(self.update_period_gui)
        self.gui_update_timer.timeout.connect(self.updateGUI)
        self.gui_update_timer.start()

        self.dataIntakeThread = DataIntakeThread(self)
        self.dataIntakeThread.start()

    def setParameters(self):
        self.parameter_queue.put(
            self.positioning_window.updated_parameter_dict)
        for parameter, value in self.positioning_window.updated_parameter_dict.items():
            self.current_parameter_dict[parameter] = value
            palette = QPalette()
            self.positioning_window.parameter_line_edits[
                parameter].setPalette(palette)

        self.positioning_window.updated_parameter_dict = {}

    def processTagDatagram(self, datagram):
        tag_id = hex(datagram["tag_id"])
        # print(tag_id)
        if not (tag_id in self.tag_dict):
            self.addTag(tag_id)
        self.tag_dict[tag_id].updateProperties(datagram)

    def processSyncDatagram(self, datagram):
        for tag_id, tag in self.tag_dict.items():
            tag.updateDiagnostics()

        for anchor_id, syncness in datagram.items():
            self.anchor_dict[anchor_id].syncness.insert(0, syncness)
            del self.anchor_dict[anchor_id].syncness[-1]

    def updatePlottedTags(self):
        for tag_id in list(self.diagnostics_window.tag_listwidget_items):
            item = self.diagnostics_window.tag_listwidget_items[tag_id]
            tag = self.tag_dict[tag_id]
            tag.setPlotted(item.checkState())

    def updatePlottedAnchors(self):
        for anchor_id in list(self.diagnostics_window.anchor_listwidget_items):
            item = self.diagnostics_window.anchor_listwidget_items[anchor_id]
            anchor = self.anchor_dict[anchor_id]
            anchor.setPlotted(item.checkState())

    def addTag(self, tag_id):
        if (tag_id == '0x3'):
            tag = Tag(tag_id, self.nb_of_datapoints,
                      self.tag_colorset[self.tag_color_index], 'cart')
        else:
            tag = Tag(tag_id, self.nb_of_datapoints,
                      self.tag_colorset[self.tag_color_index], 'basket')

        self.tag_dict[tag.id] = tag
        list_item = QListWidgetItem(tag.id)
        list_item.setCheckState(2)
        self.positioning_window.tag_listwidget_items[
            tag.id] = list_item
        self.positioning_window.tag_listwidget.addItem(
            self.positioning_window.tag_listwidget_items[tag.id])

        list_item = QListWidgetItem(tag.id)
        list_item.setCheckState(2)
        rgb = self.tag_colorset[self.tag_color_index]
        color = QColor(rgb[0], rgb[1], rgb[2])
        list_item.setForeground(QBrush(color))
        self.diagnostics_window.tag_listwidget_items[
            tag.id] = list_item
        self.diagnostics_window.tag_listwidget.addItem(
            self.diagnostics_window.tag_listwidget_items[tag.id])

        self.tag_color_index = (self.tag_color_index +
                                1) % len(self.tag_colorset)

    def updateGUI(self):
        for tag_id in list(self.positioning_window.tag_listwidget_items):
            item = self.positioning_window.tag_listwidget_items[tag_id]
            tag = self.tag_dict[tag_id]
            tag.setVisible(item.checkState())
            if item.isSelected():
                self.positioning_window.updateTagInfo(tag)

        for tag_id in list(self.diagnostics_window.tag_listwidget_items):
            item = self.diagnostics_window.tag_listwidget_items[tag_id]
            tag = self.tag_dict[tag_id]
            if item.isSelected():
                tag.setSelectedForRssDiagnostics(True)
            else:
                tag.setSelectedForRssDiagnostics(False)

        self.diagnostics_window.updatePlots(self.tag_dict, self.anchor_dict)
        self.update()


class DataIntakeThread(QThread):

    def __init__(self, parent):
        QThread.__init__(self)
        self.parent = parent
        self.tag_queue = self.parent.tag_queue
        self.sync_queue = self.parent.sync_queue

    def run(self):
        while True:
            if not self.sync_queue.empty():
                sync_data = self.sync_queue.get_nowait()
                self.parent.processSyncDatagram(sync_data)

            tag_data = None
            while not self.tag_queue.empty():
                tag_data = self.tag_queue.get_nowait()

            if tag_data:
                self.parent.processTagDatagram(tag_data)

            self.usleep(1)

    def stop(self):
        self.terminate()


class PositioningWindow(QMainWindow):

    def __init__(self, parent=None):
        super(PositioningWindow, self).__init__(parent)
        self.parent = parent
        self.selected_tag = 0
        self.show_hyperbola_for_selected_tag = False
        self.show_track_for_selected_tag = False
        self.show_grid = False
        self.update_heatmap = False
        self.parameter_window = 0

        # create subwindow with tag list
        self.tag_list_dockwindow = QDockWidget("Tags", self)
        self.addDockWidget(Qt.RightDockWidgetArea, self.tag_list_dockwindow)
        self.tag_list_dockwindow.setFloating(False)

        self.tag_listwidget = QListWidget()
        self.tag_listwidget.itemClicked.connect(self.parent.updateGUI)
        self.tag_listwidget_items = {}
        self.tag_list_dockwindow.setWidget(self.tag_listwidget)

        # create subwindow with tag info
        self.tag_info_dockwindow = QDockWidget("Tag info", self)
        self.addDockWidget(Qt.RightDockWidgetArea, self.tag_info_dockwindow)
        self.tag_info_treewidget = QTreeWidget()
        self.tag_info_treewidget.setHeaderHidden(True)
        self.tag_info_treewidget_items = {}
        self.tag_info_dict = OrderedDict()
        self.tag_info_dict["Tag ID"] = 0x0000
        self.tag_info_dict["Position (mm)"] = [0, 0, 0]
        self.tag_info_dict["Acceleration"] = [0, 0, 0]
        self.tag_info_dict["Update rate"] = 0
        self.tag_info_dict["Packet loss"] = 0
        self.tag_info_dict["RSS"] = {}
        self.tag_info_dict["Orientation"] = {}
        self.tag_info_dict["Anchor timestamps"] = {}
        for name, value in self.tag_info_dict.items():
            name_node = QTreeWidgetItem()
            name_node.setText(0, name)
            self.tag_info_treewidget.addTopLevelItem(name_node)
            self.tag_info_treewidget_items[name] = QTreeWidgetItem()
            self.tag_info_treewidget_items[name].setText(0, str(value))
            name_node.addChild(self.tag_info_treewidget_items[name])

        self.tag_info_treewidget.expandAll()
        self.tag_info_dockwindow.setWidget(self.tag_info_treewidget)
        self.tag_info_dockwindow.setFloating(False)

        # create subwindow with extra options
        self.extra_options_dockwindow = QDockWidget("Extra options", self)
        self.addDockWidget(Qt.RightDockWidgetArea,
                           self.extra_options_dockwindow)
        self.extra_options_dockwindow.setFloating(False)

        self.extra_options_widget = QWidget()
        self.extra_options_dockwindow.setWidget(self.extra_options_widget)
        self.extra_options_pushbuttons = {}

        column_width = 200
        row_height = 30
        row_index = 0
        self.extra_options_pushbuttons["Toggle hyperbolas"] = QPushButton(
            "Toggle hyperbolas", self.extra_options_widget)
        self.extra_options_pushbuttons["Toggle hyperbolas"].setGeometry(
            0.1 * column_width, row_index * row_height, column_width, row_height)
        self.extra_options_pushbuttons[
            "Toggle hyperbolas"].clicked.connect(self.toggleShowHyperbola)
        row_index += 1
        self.extra_options_pushbuttons["Toggle tracking"] = QPushButton(
            "Toggle tracking", self.extra_options_widget)
        self.extra_options_pushbuttons["Toggle tracking"].setGeometry(
            0.1 * column_width, row_index * row_height, column_width, row_height)
        self.extra_options_pushbuttons[
            "Toggle tracking"].clicked.connect(self.toggleShowTrack)
        row_index += 1
        self.extra_options_pushbuttons["Toggle grid"] = QPushButton(
            "Toggle grid", self.extra_options_widget)
        self.extra_options_pushbuttons["Toggle grid"].setGeometry(
            0.1 * column_width, row_index * row_height, column_width, row_height)
        self.extra_options_pushbuttons[
            "Toggle grid"].clicked.connect(self.toggleShowGrid)
        row_index += 1
        self.extra_options_pushbuttons["Toggle heatmap"] = QPushButton(
            "Toggle heatmap", self.extra_options_widget)
        self.extra_options_pushbuttons["Toggle heatmap"].setGeometry(
            0.1 * column_width, row_index * row_height, column_width, row_height)
        self.extra_options_pushbuttons[
            "Toggle heatmap"].clicked.connect(self.toggleShowHeatmap)

        row_index += 1
        self.parameter_line_edits = {}
        self.updated_parameter_dict = {}
        for parameter_name, parameter_value in self.parent.current_parameter_dict.items():
            label = QLabel(parameter_name, self.extra_options_widget)
            label.setGeometry(0.1 * column_width, row_index *
                              row_height, 0.7 * column_width, row_height)
            self.parameter_line_edits[parameter_name] = QLineEdit(
                str(parameter_value), self.extra_options_widget)
            self.parameter_line_edits[parameter_name].setGeometry(
                0.8 * column_width, row_index * row_height, 0.3 * column_width, row_height)
            self.parameter_line_edits[parameter_name].textEdited.connect(
                self.parameter_changed)
            row_index += 1

        self.extra_options_pushbuttons["Set parameters"] = QPushButton(
            "Set parameters", self.extra_options_widget)
        self.extra_options_pushbuttons["Set parameters"].setGeometry(
            0.1 * column_width, row_index * row_height, column_width, row_height)
        self.extra_options_pushbuttons[
            "Set parameters"].clicked.connect(self.parent.setParameters)

        self.location_map_widget = Localisation(self)
        self.setCentralWidget(self.location_map_widget)

    def updateTagInfo(self, tag):
        if not tag == self.selected_tag:
            if self.selected_tag:
                self.selected_tag.track = []
            self.selected_tag = tag

        if self.show_track_for_selected_tag and self.selected_tag:
            self.selected_tag.track.append(
                self.selected_tag.properties["Position (mm)"])

        for property_name in self.tag_info_dict:
            if property_name == "Tag ID":
                self.tag_info_treewidget_items[
                    property_name].setText(0, tag.id)
            elif property_name in ["Anchor timestamps", "RSS"]:
                formatted_dict = ""
                for anchor_id in list(tag.properties[property_name]):
                    value = tag.properties[property_name][anchor_id]
                    formatted_dict += hex(anchor_id) + \
                        ": " + str(value) + " \n"
                self.tag_info_treewidget_items[
                    property_name].setText(0, formatted_dict)
            elif property_name in ["Orientation"]:
                formatted_dict = ""
                for direction in list(tag.properties[property_name]):
                    value = tag.properties[property_name][direction]
                    formatted_dict += str(direction) + \
                        ": " + str(value) + " \n"
                self.tag_info_treewidget_items[
                    property_name].setText(0, formatted_dict)
            else:
                self.tag_info_treewidget_items[
                    property_name].setText(0, str(tag.properties[property_name]))

    def parameter_changed(self):
        self.updated_parameter_dict = {}
        for parameter, line_edit in self.parameter_line_edits.items():
            if line_edit.text():
                if "Number" in parameter:
                    try:
                        value = int(line_edit.text())
                        if value - self.parent.current_parameter_dict[parameter]:
                            self.updated_parameter_dict[
                                parameter] = int(line_edit.text())
                            palette = QPalette()
                            palette.setColor(QPalette.Text, Qt.red)
                            line_edit.setPalette(palette)
                        else:
                            palette = QPalette()
                            line_edit.setPalette(palette)
                    except:
                        error = QErrorMessage()
                        error.showMessage("The value has to be an integer")
                        error.exec_()
                        line_edit.setText(
                            str(self.parent.current_parameter_dict[parameter]))
                        palette = QPalette()
                        line_edit.setPalette(palette)
                else:
                    try:
                        value = float(line_edit.text())
                        if value - self.parent.current_parameter_dict[parameter]:
                            self.updated_parameter_dict[parameter] = value
                            palette = QPalette()
                            palette.setColor(QPalette.Text, Qt.red)
                            line_edit.setPalette(palette)
                        else:
                            palette = QPalette()
                            line_edit.setPalette(palette)
                    except:
                        error = QErrorMessage()
                        error.showMessage("The value has to be a float")
                        error.exec_()
                        line_edit.setText(
                            str(self.parent.current_parameter_dict[parameter]))
                        palette = QPalette()
                        line_edit.setPalette(palette)

    def toggleShowHyperbola(self):
        self.show_hyperbola_for_selected_tag = not self.show_hyperbola_for_selected_tag

    def toggleShowTrack(self):
        self.show_track_for_selected_tag = not self.show_track_for_selected_tag
        if self.selected_tag:
            self.selected_tag.track = []

    def toggleShowGrid(self):
        self.show_grid = not self.show_grid

    def toggleShowHeatmap(self):
        self.update_heatmap = not self.update_heatmap

class HeatMap():
    def __init__(self, x0, y0, x1, y1, r, x_n, y_n, max_hist_size):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.r = r
        self.x_n = x_n
        self.y_n = y_n
        self.max_hist_size = max_hist_size
        self.x_step = (x1 - x0) / x_n
        self.y_step = (y1 - y0) / y_n
        self.reset()

    def reset(self):
        self.data = []

    def add_point(self, new_x, new_y):
        self.data.append((new_x, new_y))
        while len(self.data)>self.max_hist_size:
            self.data.pop(0)

    def get_heatmap(self):
        kx = (self.x_n - 1) / (self.x1 - self.x0)
        ky = (self.y_n - 1) / (self.y1 - self.y0)
        img = np.zeros((self.x_n,self.y_n))
        for x, y in self.data:
            ix = int((x - self.x0) * kx)
            iy = int((y - self.y0) * ky)
            if 0 <= ix < self.x_n and 0 <= iy < self.y_n:
                img[ix][iy] += 1
        return np.log(1.+np.log(1.+ndi.gaussian_filter(img, (self.r * kx, self.r *ky), mode='constant', cval=0)))  # gaussian convolution

class Tag():

    def __init__(self, tag_id, nb_of_datapoints, color, cart_type):
        self.id = tag_id
        self.is_visible = True
        self.is_plotted = True
        self.is_selected_for_rss_diagnostics = False
        self.color = color
        self.pen = pg.mkPen(color=color, width=2)
        self.cart_type = cart_type

        self.properties = {}
        self.properties["Position (mm)"] = [0, 0, 0]
        self.properties["Acceleration"] = [0, 0, 0]
        self.properties["Update rate"] = 0
        self.properties["RSS"] = {}
        self.properties["Packet loss"] = 0
        self.properties["Orientation"] = {"Yaw": 0, "Pitch": 0}
        self.properties["Anchor timestamps"] = {}

        self.track = []

        self.nb_of_datapoints = nb_of_datapoints
        self.diagnostics = {"RSS":  {}, "Update rate":  [
            0 for i in range(self.nb_of_datapoints)], "Packet loss":  [0 for i in range(self.nb_of_datapoints)]}

    def updateProperties(self, datagram):
        self.properties["Position (mm)"] = [datagram["x"], datagram[
            "y"], datagram["z"]]
        self.properties["Acceleration"] = [
            datagram["a_x"], datagram["a_y"], datagram["a_z"]]
        self.properties["Update rate"] = datagram["update_rate"]
        self.properties["RSS"] = datagram["rss"]
        self.properties["Packet loss"] = datagram["packet_loss"]
        self.properties["Orientation"] = datagram["orientation"]
        self.properties["Anchor timestamps"] = datagram["anchor_times"]

    def updateDiagnostics(self):

        self.diagnostics["Update rate"].insert(
            0, self.properties["Update rate"])
        del self.diagnostics["Update rate"][-1]
        self.diagnostics["Packet loss"].insert(
            0, self.properties["Packet loss"])
        del self.diagnostics["Packet loss"][-1]

        for anchor_id in list(self.properties["RSS"]):
            rss_value = self.properties["RSS"][anchor_id]
            if anchor_id in self.diagnostics["RSS"]:
                self.diagnostics["RSS"][anchor_id].insert(0, rss_value)
                if (len(self.diagnostics["RSS"][anchor_id]) > self.nb_of_datapoints):
                    del self.diagnostics["RSS"][anchor_id][-1]
            else:
                self.diagnostics["RSS"][anchor_id] = [
                    0 for i in range(self.nb_of_datapoints)]
                self.diagnostics["RSS"][anchor_id][0] = rss_value

    def setVisible(self, is_checked):
        if(is_checked == 0):
            self.is_visible = False
        else:
            self.is_visible = True

    def setPlotted(self, is_checked):
        if(is_checked == 0):
            self.is_plotted = False
        else:
            self.is_plotted = True

    def setSelectedForRssDiagnostics(self, is_selected):
        self.is_selected_for_rss_diagnostics = is_selected


class Anchor():

    def __init__(self, anchor_id, position, nb_of_datapoints, color, image_angle):
        self.id = anchor_id
        self.is_plotted = True
        self.position = position
        self.image_angle = image_angle
        self.color = color
        self.pen = pg.mkPen(color=color, width=2)

        self.nb_of_datapoints = nb_of_datapoints
        self.syncness = [0 for i in range(self.nb_of_datapoints)]

    def setPlotted(self, is_checked):
        if(is_checked == 0):
            self.is_plotted = False
        else:
            self.is_plotted = True


class Product():

    def __init__(self, product_name, position, zone):
        self.name = product_name
        self.position = position
        self.zone = zone

    def isCustomerInProductZone(self, customer_position):
        return customer_position[0] > self.zone[0] and customer_position[0] < self.zone[2] and customer_position[1] > self.zone[1] and customer_position[1] < self.zone[3]


class Localisation(QWidget):

    def __init__(self, parent):
        super(Localisation, self).__init__()
        self.parent = parent
        self.heatmap_instance = self.parent.parent.heatmap
        self.current_heatmap = None
        self.heatmap_refresh_interval = 10
        self.heatmap_refresh_count = self.heatmap_refresh_interval
        self.offset_x_px = 50
        self.offset_y_px = 50
        self.anchor_dict = parent.parent.anchor_dict

        self.tag_size_mm = 2000
        self.cart_height_mm = 1170
        self.cart_width_mm = 1170
        self.basket_height_mm = 1000
        self.basket_width_mm = 848
        self.anchor_size_mm = 1000
        self.map_size_x_mm = 0
        self.map_size_y_mm = 0
        self.product_height_mm = 1380
        self.product_width_mm = 2500

        for anchor_id, anchor in self.anchor_dict.items():
            if anchor.position[0] > self.map_size_x_mm:
                self.map_size_x_mm = anchor.position[0]
            if anchor.position[1] > self.map_size_y_mm:
                self.map_size_y_mm = anchor.position[1]

        self.mm_per_grid = 1000
        self.num_gridlines_x = int(self.map_size_x_mm / self.mm_per_grid) + 1
        self.num_gridlines_y = int(self.map_size_y_mm / self.mm_per_grid) + 1

        self.pixels_per_mm = min((self.size().width() - 2 * self.offset_x_px) / self.map_size_x_mm,
                                 (self.size().height() - 2 * self.offset_y_px) / self.map_size_y_mm)

        self.cart_image = QImage()
        self.cart_image.load("gui/fig/Cart.png")
        self.cart_image_resized = self.cart_image.scaled(int(
            self.cart_width_mm * self.pixels_per_mm), int(self.cart_height_mm * self.pixels_per_mm))
        self.basket_image = QImage()
        self.basket_image.load("gui/fig/Basket.png")
        self.basket_image_resized = self.basket_image.scaled(int(
            self.basket_width_mm * self.pixels_per_mm), int(self.basket_height_mm * self.pixels_per_mm))

        self.product_image_dict = {}
        self.product_image_resized_dict = {}
        for product_name in self.parent.parent.product_dict:
            self.product_image_dict[product_name] = QImage()
            self.product_image_dict[product_name].load("gui/fig/" + product_name + ".png")
            self.product_image_resized_dict[product_name] =self.product_image_dict[product_name].scaled(int(
                self.product_width_mm  * self.pixels_per_mm), int(self.product_height_mm * self.pixels_per_mm))

        self.anchor_image_dict = {}
        self.anchor_image_resized_dict = {}
        self.anchor_image_dict[0] = QImage()
        self.anchor_image_dict[0].load("gui/fig/Anchor_Lower_Left.png")
        self.anchor_image_resized_dict[0] = self.anchor_image_dict[0].scaled(int(
            self.anchor_size_mm * self.pixels_per_mm), int(self.anchor_size_mm * self.pixels_per_mm))
        self.anchor_image_dict[90] = QImage()
        self.anchor_image_dict[90].load("gui/fig/Anchor_Upper_Left.png")
        self.anchor_image_resized_dict[90] = self.anchor_image_dict[90].scaled(int(
            self.anchor_size_mm * self.pixels_per_mm), int(self.anchor_size_mm * self.pixels_per_mm))
        self.anchor_image_dict[180] = QImage()
        self.anchor_image_dict[180].load("gui/fig/Anchor_Upper_Right.png")
        self.anchor_image_resized_dict[180] = self.anchor_image_dict[180].scaled(int(
            self.anchor_size_mm * self.pixels_per_mm), int(self.anchor_size_mm * self.pixels_per_mm))
        self.anchor_image_dict[270] = QImage()
        self.anchor_image_dict[270].load("gui/fig/Anchor_Lower_Right.png")
        self.anchor_image_resized_dict[270] = self.anchor_image_dict[270].scaled(int(
            self.anchor_size_mm * self.pixels_per_mm), int(self.anchor_size_mm * self.pixels_per_mm))

        self.shop_image = QImage()
        # self.shop_image.load("gui/fig/Industrial_Background.png")
        self.shop_image.load("gui/fig/ColruytBackground.png")

    def resizeEvent(self, event):
        self.pixels_per_mm = min((self.size().width() - 2 * self.offset_x_px) / self.map_size_x_mm,
                                 (self.size().height() - 2 * self.offset_y_px) / self.map_size_y_mm)
        self.cart_image_resized = self.cart_image.scaled(int(
            self.cart_width_mm * self.pixels_per_mm), int(self.cart_height_mm * self.pixels_per_mm))
        self.basket_image_resized = self.basket_image.scaled(int(
            self.basket_width_mm * self.pixels_per_mm), int(self.basket_height_mm * self.pixels_per_mm))

        for product_name in self.product_image_dict:
            self.product_image_resized_dict[product_name] = self.product_image_dict[product_name].scaled(int(
                self.product_width_mm  * self.pixels_per_mm), int(self.product_height_mm * self.pixels_per_mm))

        for angle in self.anchor_image_dict:
            self.anchor_image_resized_dict[angle] = self.anchor_image_dict[angle].scaled(int(
                self.anchor_size_mm * self.pixels_per_mm), int(self.anchor_size_mm * self.pixels_per_mm))

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.updateLocations(qp)
        qp.end()

    def updateLocations(self, qp):

        size = self.size()
        self.drawShop(qp)
        if self.parent.show_grid:
            self.drawGrid(qp)

        transformation = QTransform()
        qp.setTransform(transformation)
        for anchor_id in dict(self.parent.parent.anchor_dict):
            anchor = self.parent.parent.anchor_dict[anchor_id]
            x_mm = anchor.position[0] - 0.5 * self.anchor_size_mm
            y_mm = anchor.position[1] + 0.5 * self.anchor_size_mm
            (x, y) = self.convertToQtPosition(x_mm, y_mm)
            qp.drawImage(x, y, self.anchor_image_resized_dict[
                         anchor.image_angle])

        qp.setPen(QPen(QBrush(Qt.darkGreen), 6))
        for tag_id in dict(self.parent.parent.tag_dict):
            tag = self.parent.parent.tag_dict[tag_id]
            if tag.is_visible:
                rotation_angle = tag.properties["Orientation"]["Yaw"]
                x_mm = tag.properties["Position (mm)"][0]
                y_mm = tag.properties["Position (mm)"][1]
                (x, y) = self.convertToQtPosition(x_mm, y_mm)
                self.parent.parent.heatmap.add_point(x_mm, y_mm)

                if (tag.cart_type == 'cart'):
                    transformation = QTransform()
                    transformation.translate(x, y)
                    transformation.rotate(rotation_angle)
                    transformation.translate(-0.5 * self.cart_width_mm *
                                             self.pixels_per_mm, -0.9 * self.cart_height_mm * self.pixels_per_mm)
                    qp.setTransform(transformation)
                    qp.drawImage(0, 0, self.cart_image_resized)
                else:
                    transformation = QTransform()
                    transformation.translate(x, y)
                    transformation.rotate(rotation_angle)
                    transformation.translate(-0.5 * self.basket_width_mm *
                                             self.pixels_per_mm, -0.5 * self.basket_height_mm * self.pixels_per_mm)
                    qp.setTransform(transformation)
                    qp.drawImage(0, 0, self.basket_image_resized)

                transformation = QTransform()
                qp.setTransform(transformation)
                qp.drawEllipse(x - 10, y - 10, 20, 20)

                if (tag.cart_type == 'cart'):
                    for product_name, product in self.parent.parent.product_dict.items():
                        if product.isCustomerInProductZone([x_mm, y_mm]):
                            x_mm = product.position[0] - 0.5 * self.product_height_mm
                            y_mm = product.position[1] + 0.5 * self.product_width_mm
                            (x, y) = self.convertToQtPosition(x_mm, y_mm)
                            qp.drawImage(x, y, self.product_image_resized_dict[product_name])

        if self.parent.update_heatmap:
            self.drawHeatMap(qp)


        if (self.parent.show_hyperbola_for_selected_tag and self.parent.selected_tag):
            for anchor1_id in list(self.parent.selected_tag.properties["Anchor timestamps"]):
                for anchor2_id in list(self.parent.selected_tag.properties["Anchor timestamps"]):
                    if anchor1_id > anchor2_id:
                        anchor1 = self.parent.parent.anchor_dict[anchor1_id]
                        anchor2 = self.parent.parent.anchor_dict[anchor2_id]
                        anchor1_time_mm = self.parent.selected_tag.properties[
                            "Anchor timestamps"][anchor1_id]
                        anchor2_time_mm = self.parent.selected_tag.properties[
                            "Anchor timestamps"][anchor2_id]
                        self.drawHyperbola(
                            qp, anchor1, anchor2, anchor1_time_mm, anchor2_time_mm)

        transformation = QTransform()
        qp.setTransform(transformation)
        if (self.parent.show_track_for_selected_tag and self.parent.selected_tag):
            self.drawTrack(qp, self.parent.selected_tag.track)

    def drawShop(self, qp):
        (x, y) = self.convertToQtPosition(0, self.map_size_y_mm)
        image = self.shop_image.scaled(int(
            self.map_size_x_mm * self.pixels_per_mm), int(self.map_size_y_mm * self.pixels_per_mm))
        qp.drawImage(x, y, image)

    def drawGrid(self, qp):
        size = self.frameGeometry()
        increment = int(self.mm_per_grid * self.pixels_per_mm)
        qp.setPen(QPen(QBrush(Qt.black), 1))
        # vertical
        for x_pos_mm in range(self.mm_per_grid, self.map_size_x_mm, self.mm_per_grid):
            (x_from, y_from) = self.convertToQtPosition(x_pos_mm, 0)
            (x_to, y_to) = self.convertToQtPosition(
                x_pos_mm, self.map_size_y_mm)
            qp.drawLine(x_from, y_from, x_to, y_to)
        # horizontal
        for y_pos_mm in range(self.mm_per_grid, self.map_size_y_mm, self.mm_per_grid):
            (x_from, y_from) = self.convertToQtPosition(0, y_pos_mm)
            (x_to, y_to) = self.convertToQtPosition(
                self.map_size_x_mm, y_pos_mm)
            qp.drawLine(x_from, y_from, x_to, y_to)

        qp.setPen(QPen(QBrush(Qt.black), 6))
        (x_from, y_from) = self.convertToQtPosition(0, self.map_size_y_mm)
        qp.drawRect(x_from, y_from, self.map_size_x_mm *
                    self.pixels_per_mm, self.map_size_y_mm * self.pixels_per_mm)

    def drawHeatMap(self, qp):
        if self.heatmap_refresh_count == self.heatmap_refresh_interval:
            self.current_heatmap = self.heatmap_instance.get_heatmap()
            self.heatmap_refresh_count = 0
        else:
            self.heatmap_refresh_count += 1

        x_step = self.heatmap_instance.x_step
        y_step = self.heatmap_instance.y_step
        current_heatmap_maximum = np.max(self.current_heatmap)
        for x_block in range(self.heatmap_instance.x_n):
            for y_block in range(self.heatmap_instance.y_n):
                (x_from, y_from) = self.convertToQtPosition(x_block * x_step, y_block * y_step)
                (x_to, y_to) = self.convertToQtPosition((x_block + 1)* x_step, (y_block + 1) * y_step)
                width = x_to - x_from
                height = y_to - y_from
                color = QColor()
                if current_heatmap_maximum:
                    hue = int(120 - 120 * self.current_heatmap[x_block, y_block]/current_heatmap_maximum)
                else:
                    hue = 120
                color.setHsv(hue, 255, 255, 50)
                qp.fillRect(x_from, y_from, width, height, color)

    def drawHyperbola(self, qp, anchor1, anchor2, anchor1_time_mm, anchor2_time_mm):
        # sqrt((x+c)^2 + y^2) - sqrt((x-c)^2 + y^2) = 2d reduces to
        # x^2/ a^2 - y^2 / b^2 = 1                       with a² = d² and b²= c² - d²
        # y = sqrt((b^2)/(a^2) x^2 - b^2)

        time_difference_mm = anchor1_time_mm - anchor2_time_mm
        qp.setPen(QPen(QBrush(Qt.blue), 2))
        delta_x = anchor1.position[0] - anchor2.position[0]
        delta_y = anchor1.position[1] - anchor2.position[1]
        delta = np.sqrt(delta_x**2 + delta_y**2)
        c = int(delta / 2 * self.pixels_per_mm)
        d = int(time_difference_mm / 2 * self.pixels_per_mm)
        a = np.abs(d)
        b_squared = c**2 - d**2
        painter_path_upper = QPainterPath()
        painter_path_upper.moveTo(a, 0)
        painter_path_lower = QPainterPath()
        painter_path_lower.moveTo(a, 0)

        for x in range(a, c):
            if a**2:
                painter_path_upper.lineTo(
                    x, - int(np.sqrt(np.abs((b_squared) / (a**2) * x**2 - b_squared))))

        for x in range(a, c):
            if a**2:
                painter_path_lower.lineTo(
                    x,  int(np.sqrt(np.abs((b_squared) / (a**2) * x**2 - b_squared))))

        x_mm = (anchor1.position[0] + anchor2.position[0]) / 2
        y_mm = (anchor1.position[1] + anchor2.position[1]) / 2
        (x, y) = self.convertToQtPosition(x_mm, y_mm)
        transformation = QTransform()
        transformation.translate(x, y)
        angle = - math.atan2(delta_y, delta_x)
        if d > 0:
            angle += math.pi
        transformation.rotateRadians(angle)
        qp.setTransform(transformation)

        qp.drawPath(painter_path_upper)
        qp.drawPath(painter_path_lower)

    def drawTrack(self, qp, positions):
        (x, y) = self.convertToQtPosition(positions[0][0], positions[0][1])
        track_path = QPainterPath()
        track_path.moveTo(x, y)
        for position in positions:
            (x, y) = self.convertToQtPosition(position[0], position[1])
            track_path.lineTo(x, y)

        qp.setPen(QPen(QBrush(Qt.green), 2))
        qp.drawPath(track_path)

    def convertToQtPosition(self, x_mm, y_mm):
        new_x = int(x_mm * self.pixels_per_mm + self.offset_x_px)
        new_y = int(self.size().height() -
                    (y_mm * self.pixels_per_mm + self.offset_y_px))
        return(new_x, new_y)


class DiagnosticsWindow(QMainWindow):

    def __init__(self,  parent=None):
        super(DiagnosticsWindow, self).__init__(parent)
        self.parent = parent
        self.time = [
            i * self.parent.current_parameter_dict["Sync period"] for i in range(self.parent.nb_of_datapoints)]

        # create subwindow with tag list
        self.tag_list_dockwindow = QDockWidget("Tags", self)
        self.addDockWidget(Qt.TopDockWidgetArea, self.tag_list_dockwindow)
        self.tag_list_dockwindow.setFloating(False)

        self.tag_listwidget = QListWidget()
        self.tag_listwidget.itemClicked.connect(self.parent.updatePlottedTags)
        self.tag_listwidget_items = {}
        self.tag_list_dockwindow.setWidget(self.tag_listwidget)

        # create subwindow with tag list
        self.anchor_list_dockwindow = QDockWidget("Anchors", self)
        self.addDockWidget(Qt.TopDockWidgetArea, self.anchor_list_dockwindow)
        self.anchor_list_dockwindow.setFloating(False)

        self.anchor_listwidget = QListWidget()
        self.anchor_listwidget.itemClicked.connect(
            self.parent.updatePlottedAnchors)
        self.anchor_listwidget_items = {}
        self.anchor_list_dockwindow.setWidget(self.anchor_listwidget)
        for anchor_id, anchor in parent.anchor_dict.items():
            self.anchor_listwidget_items[
                anchor_id] = QListWidgetItem(hex(anchor_id))
            self.anchor_listwidget.addItem(
                self.anchor_listwidget_items[anchor_id])
            self.anchor_listwidget_items[anchor_id].setCheckState(2)
            rgb = anchor.color
            color = QColor(rgb[0], rgb[1], rgb[2])
            self.anchor_listwidget_items[
                anchor_id].setForeground(QBrush(color))

        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')
        self.diagnostics_plot_widget = GraphicsWindow()
        self.rss_subplot = self.diagnostics_plot_widget.addPlot(row=0, col=0)
        self.packet_loss_subplot = self.diagnostics_plot_widget.addPlot(
            row=0, col=1)
        self.update_rate_subplot = self.diagnostics_plot_widget.addPlot(
            row=1, col=0)
        self.syncness_subplot = self.diagnostics_plot_widget.addPlot(
            row=1, col=1)
        self.setCentralWidget(self.diagnostics_plot_widget)

        # self.rss_subplot.addLegend()
        self.rss_subplot.setWindowTitle('RSS')
        self.rss_subplot.setLabel('bottom', text='Time (s)')
        self.rss_subplot.setLabel('left', text='RSS (dB)')
        # self.packet_loss_subplot.addLegend()
        self.packet_loss_subplot.setWindowTitle('Packet loss')
        self.packet_loss_subplot.setLabel('bottom', text='Time (s)')
        self.packet_loss_subplot.setLabel('left', text='Packet loss')
        # self.update_rate_subplot.addLegend()
        self.update_rate_subplot.setWindowTitle('Update rate')
        self.update_rate_subplot.setLabel('bottom', text='Time (s)')
        self.update_rate_subplot.setLabel('left', text='Update rate (Hz)')
        # self.syncness_subplot.addLegend()
        self.syncness_subplot.setWindowTitle('Syncness')
        self.syncness_subplot.setLabel('bottom', text='Time (s)')
        self.syncness_subplot.setLabel('left', text='Drift per sync (mm)')

    def updatePlots(self, tag_dict, anchor_dict):
        self.time = [i * self.parent.current_parameter_dict["Sync period"]
                     for i in range(self.parent.nb_of_datapoints)]
        self.rss_subplot.clear()
        # self.rss_subplot.addLegend()
        self.update_rate_subplot.clear()
        # self.update_rate_subplot.addLegend()
        self.packet_loss_subplot.clear()
        # self.packet_loss_subplot.addLegend()

        for tag_id in list(tag_dict):
            tag = tag_dict[tag_id]
            if tag.is_plotted:
                if tag.is_selected_for_rss_diagnostics:
                    anchor_pen_index = 0
                    for anchor_id in list(tag.diagnostics["RSS"]):
                        rss_values = tag.diagnostics["RSS"][anchor_id]
                        if anchor_dict[anchor_id].is_plotted:
                            self.rss_subplot.plot(x=self.time, y=rss_values, pen=anchor_dict[
                                                  anchor_id].pen, name=str(anchor_id))
                            anchor_pen_index += 1

                self.update_rate_subplot.plot(x=self.time, y=tag.diagnostics[
                    "Update rate"], pen=tag.pen, name=str(tag_id))

                self.packet_loss_subplot.plot(x=self.time, y=tag.diagnostics[
                    "Packet loss"], pen=tag.pen, name=str(tag_id))

        self.syncness_subplot.clear()
        # self.syncness_subplot.addLegend()
        anchor_pen_index = 0
        for anchor_id in list(anchor_dict):
            anchor = anchor_dict[anchor_id]
            if anchor.is_plotted:
                self.syncness_subplot.plot(x=self.time, y=anchor.syncness, pen=anchor.pen,
                                           name=str(anchor_id))
                anchor_pen_index += 1
