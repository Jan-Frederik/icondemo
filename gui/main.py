from multiprocessing import Process, Queue
import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from TDOAGui import TdoaMainWindow
from shout import shout

def spawnGui(taq_queue, sync_queue, parameter_queue):
   app = QApplication(sys.argv)
   ex = TdoaMainWindow(taq_queue, sync_queue, parameter_queue)
   ex.show()
   sys.exit(app.exec_())

if __name__ == '__main__':
    taq_queue = Queue()
    sync_queue = Queue()
    parameter_queue = Queue()

    p1 = Process(target=shout, args=(taq_queue, sync_queue, parameter_queue))
    p1.start()

    p2 = Process(target=spawnGui, args=(taq_queue, sync_queue, parameter_queue))
    p2.start()
