import socket
import struct
from random import randint
from time import sleep, time


def shout(taq_queue, sync_queue, parameter_queue):
    struc = struct.Struct(">hIii")
    anchorId = [0x1001, 0x1002, 0x1003, 0x1004, 0x1005, 0x1006]

    freq_total = 10
    sets = 2
    time_delay = 1 / (freq_total * sets)

    i = 0
    while True:
        i += 1
        sleep(time_delay)
        x = [randint(2600, 2700), randint(1900, 2100), randint(400, 600), randint(
            8000, 8200), randint(5500, 5700), randint(7900, 8100)]
        y = [randint(4000, 4300), randint(2900, 3100), randint(4600, 4800), randint(
            3500, 3900), randint(3500, 3900), randint(1800, 2000)]
        z = [randint(1750, 1950), randint(2040, 2070), randint(1230, 1570), randint(
            1400, 1570), randint(1680, 1900), randint(1800, 2000)]
        a_x = [randint(400, 430), randint(290, 310), randint(460, 480), randint(
            350, 390), randint(350, 390), randint(180, 200)]
        a_y = [randint(175, 195), randint(204, 207), randint(123, 157), randint(
            140, 157), randint(168, 190), randint(180, 200)]
        a_z = [randint(260, 270), randint(190, 210), randint(40, 60), randint(
            800, 820), randint(550, 570), randint(790, 810)]
        sigma = [randint(1345, 1456) / 1000, randint(1424, 1624) / 1000, randint(1454, 1564) / 1000, randint(
            1500, 1900) / 1000, randint(3500, 3900) / 1000, randint(1800, 2000) / 1000]
        rss = [randint(80, 100), randint(80, 100), randint(80, 100), randint(80, 100),
               randint(80, 100), randint(80, 100)]
        update_rate = [randint(9, 11), randint(9, 11), randint(
            9, 11), randint(9, 11), randint(9, 11), randint(9, 11)]
        packet_loss = [randint(0, 20) / 100, randint(0, 20) / 100, randint(
            0, 20) / 100, randint(0, 20) / 100, randint(0, 20) / 100, randint(0, 20) / 100]
        yaw = [randint(0, 360), randint(0, 360),randint(0, 360),randint(0, 360),randint(0, 360),randint(0, 360)]
        data = {"tag_id": anchorId[i % 6], "x": x[i % 6], "y": y[i % 6], "z": z[i % 6], "a_x": a_x[i % 6],
                "a_y": a_y[i % 6], "a_z": a_z[i % 6], "sigma": sigma[i % 6], "rss": {0xA001: rss[1], 0x0A002: rss[2], 0xA003: rss[3], 0xA004: rss[4]},
                "update_rate": update_rate[i % 6], "packet_loss": packet_loss[i % 6], "orientation":{"Yaw": yaw[i % 6], "Pitch": yaw[i % 6]},
                "anchor_times": {0xA001: 4493730634160 + randint(0, 1000), 0x0A002: 4493730640440 + randint(0, 1000), 0xA003: 4493730641280 + randint(0, 1000), 0xA004: 4493730637170 + randint(0, 1000)}}
        taq_queue.put(data)

        syncness = [randint(400, 430) / 1000, randint(290, 310) /
                    1000, randint(460, 480) / 1000, randint(350, 390) / 1000]
        if (i % int(1/ time_delay) == 0):
            sync_data = {0xA001: syncness[0], 0xA002: syncness[1], 0xA003: syncness[2], 0xA004: syncness[3]}
            sync_queue.put(sync_data)

        if not parameter_queue.empty():
            print(parameter_queue.get_nowait())
