NUM_ANCHORS = 4
PRINT_DEBUG_MESSAGES = False


class AggregatedPackets(object):
    AMOUNT = 256*256
    NUM_BEFORE_SEND = NUM_ANCHORS
    DEBUG_MESSAGE_RX = "PACKET HAS BEEN RECEIVED"
    DEBUG_MESSAGE_TX = "AGGREGATED PACKET HAS BEEN SENT"
    NEEDS_MASTER = False

    def __init__(self, tx_id, send_queue):
        self.tx_id = tx_id
        self.packets = [None] * self.AMOUNT
        self.send_queue = send_queue

    def try_send_packet(self, index):
        if 'rx_data' in self.packets[index] and len(self.packets[index]['rx_data']) == self.NUM_BEFORE_SEND:
            if not self.NEEDS_MASTER or 'tx_timestamp' in self.packets[index]:
                if PRINT_DEBUG_MESSAGES:
                    print(self.DEBUG_MESSAGE_TX)
                self.send_queue.put(self.packets[index])
                if PRINT_DEBUG_MESSAGES:
                    print(self.packets[index]['extras'])
                    print("Packet put on queue")
                self.clear_previous_unsent_packets(index)

                # maybe use a growing row instead so that old packets have no chance of reincarnating?

    def clear_previous_unsent_packets(self, index):
        clear_amount = int(self.AMOUNT / 2)
        if index < clear_amount:
            previous_clear_index = self.AMOUNT - (clear_amount - index)
            #print('index', index, 'test1', (clear_amount - index), self.AMOUNT - (clear_amount - index))
            self.packets[previous_clear_index:self.AMOUNT] = [None] * (self.AMOUNT - previous_clear_index)
            self.packets[0:index] = [None] * index
        else:
            self.packets[index - clear_amount:index] = [None] * clear_amount
        self.packets[index] = None

    def add_rx_packet(self, index, rx_id, rx_timestamp, rx_diagnostics, extras):
        if PRINT_DEBUG_MESSAGES:
            print(self.DEBUG_MESSAGE_RX)
        if self.packets[index] is None:
            self.packets[index] = {
                'index': index,
                'tx_id': self.tx_id,
                'rx_data': [{'rx_id': rx_id, 'timestamp': rx_timestamp, 'diagnostics': rx_diagnostics}],
                'extras': extras
            }
        else:
            if 'rx_data' in self.packets[index]:
                for rx_data in self.packets[index]['rx_data']:
                    if rx_id == rx_data['rx_id']:
                        return
                self.packets[index]['rx_data'].append({'rx_id': rx_id, 'timestamp': rx_timestamp, 'diagnostics': rx_diagnostics})

            else:
                self.packets[index]['rx_data'] = [{'rx_id': rx_id, 'timestamp': rx_timestamp, 'diagnostics': rx_diagnostics}]
            if 'extras' not in self.packets[index]:
                self.packets[index]['extras'] = extras

        self.try_send_packet(index)

    def add_tx_packet(self, index, tx_timestamp):
        if PRINT_DEBUG_MESSAGES:
            print("MASTER " + self.DEBUG_MESSAGE_RX)
        if self.packets[index] is None:
            self.packets[index] = {
                'index': index,
                'tx_id': self.tx_id,
                'tx_timestamp': tx_timestamp,
            }

        self.try_send_packet(index)


class Blinks(AggregatedPackets):
    NUM_BEFORE_SEND = NUM_ANCHORS
    DEBUG_MESSAGE_RX = "BLINK PACKET HAS BEEN RECEIVED"
    DEBUG_MESSAGE_TX = "AGGREGATED BLINK HAS BEEN SENT"


class Syncs(AggregatedPackets):
    NUM_BEFORE_SEND = NUM_ANCHORS - 1
    NEEDS_MASTER = True
    DEBUG_MESSAGE_RX = "SYNC PACKET HAS BEEN RECEIVED"
    DEBUG_MESSAGE_TX = "AGGREGATED SYNC HAS BEEN SENT"
