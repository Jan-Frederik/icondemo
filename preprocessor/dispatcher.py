
from preprocessor.aggregated_packets import Blinks, Syncs
from preprocessor.packet_interpreter import PacketInterpreter


# dummy but maybe implement later
class Dispatcher(object):

    def __init__(self, socket, sync_dispatch_queue, blink_dispatch_queue):
        self.socket = socket
        self.sync_dispatch_queue = sync_dispatch_queue
        self.blink_dispatch_queue = blink_dispatch_queue

        self.all_syncs = []
        self.tx_anchor_ids = []

        self.all_blinks = []
        self.tx_tag_ids = []

    def loop(self):
        data, address = self.socket.recvfrom(1024)
        packet = PacketInterpreter(data)
        if packet.packet_id == 0:
            self.rx_packet(packet)
        elif packet.packet_id == 1:
            self.tx_packet(packet)

    def tx_packet(self, packet):
        packet_values = packet.packet_values
        if packet_values[0] not in self.tx_anchor_ids:
            self.tx_anchor_ids.append(packet_values[0])
            self.all_syncs.append(Syncs(packet_values[0], self.sync_dispatch_queue))

        index, tx_timestamp = packet_values[2], packet_values[1]
        self.all_syncs[self.tx_anchor_ids.index(packet_values[0])].add_tx_packet(index, tx_timestamp)

    def rx_packet(self, packet):
        if packet.has_payload:
            if packet.payload_id == ord('S'):
                self.rx_sync(packet)
            elif packet.payload_id == ord('B'):
                self.rx_blink(packet)

    def rx_blink(self, packet):
        packet_values, payload_values = packet.packet_values, packet.payload_values
        if payload_values[0] not in self.tx_tag_ids:
            self.tx_tag_ids.append(payload_values[0])
            self.all_blinks.append(Blinks(payload_values[0], self.blink_dispatch_queue))

        index, rx_id, rx_timestamp, rx_diagnostics, extras = payload_values[
            1], packet_values[0], packet_values[1], packet.diagnostics, payload_values[2:8]

        self.all_blinks[self.tx_tag_ids.index(payload_values[0])].add_rx_packet(
            index, rx_id, rx_timestamp, rx_diagnostics, extras)

    def rx_sync(self, packet):
        packet_values, payload_values = packet.packet_values, packet.payload_values

        if payload_values[0] not in self.tx_anchor_ids:
            self.tx_anchor_ids.append(payload_values[0])
            self.all_syncs.append(Syncs(payload_values[0], self.sync_dispatch_queue))

        # data = (payload_values[2], packet_values[
        #     0], packet_values[1], packet.diagnostics, (payload_values[1], payload_values[3], payload_values[4]))
        index, rx_id, rx_timestamp, rx_diagnostics, extras = payload_values[2], packet_values[
            0], packet_values[1], packet.diagnostics, (payload_values[1], payload_values[3], payload_values[4])
        self.all_syncs[self.tx_anchor_ids.index(payload_values[0])].add_rx_packet(
            index, rx_id, rx_timestamp, rx_diagnostics, extras)

    # think it's pretty easy to decouple a lot of logic here if using * with lists. #jizz


def run_dispatcher(socket, sync_dispatch_queue, blink_dispatch_queue):
    dispatcher = Dispatcher(socket, sync_dispatch_queue, blink_dispatch_queue)
    while True:
        dispatcher.loop()


if __name__ == '__main__':
    import socket
    from multiprocessing import Queue
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = ('0.0.0.0', 8888)
    s.bind(server_address)
    run_dispatcher(socket, Queue(), Queue())
