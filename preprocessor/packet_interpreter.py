from preprocessor.pozyx_packets import (MAIN_HEADER_STRUCT, PACKETS, PAYLOAD_HEADER_STRUCT,
                           PAYLOADS, RX_DIAGNOSTICS_CONTENTS,
                           RX_DIAGNOSTICS_STRUCT)

NUM_ANCHORS = 4


class PacketInterpreter(object):

    def __init__(self, packet):
        self.packet = packet
        self.index = 0
        self.interpret_main_contents()
        self.has_payload = False
        self.interpret_payload()

    def unpack_part(self, unpacking_struct):
        data = unpacking_struct.unpack(self.packet[self.index:self.index + unpacking_struct.size])
        self.index += unpacking_struct.size
        return data

    def interpret_main_contents(self):
        self.packet_id, = self.unpack_part(MAIN_HEADER_STRUCT)
        packet_definition = PACKETS[self.packet_id % 128]
        self.diagnostics = ()
        if packet_definition is not None:
            self.packet_contents = packet_definition.contents
            if self.packet_id & 0x80:
                self.diagnostics = self.unpack_part(RX_DIAGNOSTICS_STRUCT)
                self.packet_id -= 0x80
            self.packet_values = self.unpack_part(packet_definition.struct)

    def interpret_payload(self):
        if len(self.packet) > self.index:
            self.has_payload = True
            self.payload_id, self.payload_firmware_version = self.unpack_part(PAYLOAD_HEADER_STRUCT)
            payload_definition = PAYLOADS[self.payload_id]

            if payload_definition is not None:
                self.payload_contents = payload_definition.contents
                self.payload_values = self.unpack_part(payload_definition.struct)

    def __str__(self):
        s = 'ID: {} HAS_DIAG: {} DIAG: {} CONTENT: {} '.format(
            self.packet_id % 128, self.packet_id >= 128, self.diagnostics, self.packet_values)
        try:
            s += 'PAYLOAD ID: {} FW: {} CONTENT: {}'.format(self.payload_id,
                                                            self.payload_firmware_version, self.payload_values)
        except:
            s += 'NO PAYLOAD'
        return s

if __name__ == '__main__':

    LIVE_TEST = True

    if not LIVE_TEST:
        from struct import Struct

        def test_packet(struct_format, data):
            test_struct = Struct(struct_format)
            test_packet_bytes = test_struct.pack(*data)
            print(PacketInterpreter(test_packet_bytes))

        rx_no_diag = test_packet('>BHQ', [0, 0x1000, 130001])
        rx_diag = test_packet('>BHHHHHHHHHQ', [128] + [i for i in range(8)] + [0x2000, 130002])
        rx_no_diag_blink = test_packet('>BHQBBHBhhhhhh', [0, 0x3000, 130003, ord('B'), 1, 0x4000, 1] + [i for i in range(6)])
        rx_diag_blink = test_packet('>BHHHHHHHHHQBBHBhhhhhh', [
            128] + [i for i in range(8)] + [0x3000, 130003, ord('B'), 1, 0x4000, 1] + [i for i in range(6)])
        # still to test
        # rx_no_diag_sync
        # rx_diag_sync
        # tx_no_diag_sync
        # tx_diag_sync
    else:
        print("TESTING LIVE DATA")
        import socket

        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_address = ('0.0.0.0', 8888)
        s.bind(server_address)

        data, address = s.recvfrom(1024)
        while True:
            data, address = s.recvfrom(1024)
            print(PacketInterpreter(data))
