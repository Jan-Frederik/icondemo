import json
from struct import Struct, calcsize

DEFINITIONS_FOLDER = "packet_definitions/"

MAIN_HEADER_STRUCT = Struct('<B')
PAYLOAD_HEADER_STRUCT = Struct('<BB')
RX_DIAGNOSTICS_STRUCT = Struct('<HHHHHHHH')
RX_DIAGNOSTICS_CONTENTS = ["RX_DIAGNOSTICS_1", "RX_DIAGNOSTICS_2", "RX_DIAGNOSTICS_3",
                           "RX_DIAGNOSTICS_4", "RX_DIAGNOSTICS_5", "RX_DIAGNOSTICS_6", "RX_DIAGNOSTICS_7", "RX_DIAGNOSTICS_8"]


def get_json_data(filename):
    with open(DEFINITIONS_FOLDER + filename, 'r') as fp:
        return json.load(fp)


class PacketContents(object):
    definitions_file = 'error.txt'
    contents_file = 'error.txt'

    def __init__(self, name, metadata, content_metadata):
        self.name = name
        self.identifier = metadata['identifier']

        self.contents = metadata['contents']
        self.description = metadata['description']

        self.construct_data_struct(content_metadata)

    def construct_data_struct(self, content_metadata):
        self.data = '<'
        for content in self.contents:
            self.data += content_metadata[content]
        self.struct = Struct(self.data)
        self.length = self.struct.size


class PacketMainContents(PacketContents):
    """Defines a packet, with its data size and format"""
    definitions_file = 'packets.json'
    contents_file = 'packet_contents.json'
    amount = 128

    def __init__(self, name, metadata, content_metadata):
        super(PacketMainContents, self).__init__(name, metadata, content_metadata)
        self.log_file = metadata['log_file']


class PacketPayloadContents(PacketContents):
    """Defines a payload, with its data size and format"""
    definitions_file = 'payloads_v1.0.json'
    contents_file = 'payload_contents.json'
    amount = 256


def get_definitions(packet_class):
    definitions = get_json_data(packet_class.definitions_file)
    contents = get_json_data(packet_class.contents_file)
    objects = []
    for definition in definitions:
        objects.append(packet_class(definition, definitions[definition], contents))

    sorted_objects = [None] * packet_class.amount
    for object_ in objects:
        sorted_objects[object_.identifier] = object_
    return sorted_objects

PACKETS = get_definitions(PacketMainContents)
PAYLOADS = get_definitions(PacketPayloadContents)


if __name__ == '__main__':
    datasets = [PACKETS, PAYLOADS]
    for set_ in datasets:
        for object_ in set_:
            if object_ is not None:
                print(object_.identifier, object_.description)
