#!/usr/bin/env python
"""
packet_logger

PyQt5 application which receives the UPD packets, adds its own timestamp
of packet reception, and logs it to that packet's respective file.
"""

import json
import random
import sys
from time import sleep, time

from PyQt5.QtCore import *
from PyQt5.QtNetwork import *
from PyQt5.QtWidgets import *

from packets import (PACKET_HEADER_STRUCT, PAYLOAD_HEADER_STRUCT, get_packets,
                     get_payloads)


class PacketLogger(QWidget):

    def __init__(self):
        super(PacketLogger, self).__init__()

        self.packets = get_packets('packets.json')
        self.payloads = get_payloads('payloads_v0.1.json')
        # log files hack
        self.files = [None] * len(self.packets)
        for packet in self.packets:
            if packet is not None:
                self.files[packet.identifier] = open('logs/' + packet.log_file, 'a')

        # bind callback for UDP packets
        self.socket = QUdpSocket(self)
        self.socket.bind(QHostAddress('0.0.0.0'), 8888)
        self.socket.readyRead.connect(self.readPendingDatagrams)

        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 290, 150)
        self.setWindowTitle('Logger')
        self.show()

    def readPendingDatagrams(self):
        while self.socket.hasPendingDatagrams():
            data, _, _ = self.socket.readDatagram(self.socket.pendingDatagramSize())
            self.processDatagram(data)
        self.update()

    def processDatagram(self, datagram):
        # make header size also modular
        identifier, = PACKET_HEADER_STRUCT.unpack(datagram[0])
        packet = self.packets[identifier % 128]
        if packet is not None:
            try:
                if (identifier / 128) >= 1:
                    contents = packet.struct_diag.unpack(datagram[1:packet.length_diag])
                    if len(datagram) > packet.length_diag + 1:
                        contents += self.processPayload(datagram[packet.length_diag:])
                else:
                    contents = packet.struct.unpack(datagram[1:packet.length])
                    if len(datagram) > packet.length + 1:
                        contents += self.processPayload(datagram[packet.length:])

                print(identifier % 128, packet.name, contents)
                self.files[identifier].write('T %i %s\n' % (time() * 1000, str(contents)))
            except:
                print("Packet ID %i with size %i didn't match specifications" %
                      (identifier % 128, len(datagram) - 2))

    def processPayload(self, payload_datagram):
        identifier, firmware_version = PAYLOAD_HEADER_STRUCT.unpack(payload_datagram[0:2])
        # add firmware version differentiator here
        payload = self.payloads[identifier]
        if payload is not None:
            try:
                return payload.struct.unpack(payload_datagram[2:])
            except:
                print("Payload didn't meet specifications")
        else:
            print("Payload has not been defined")
            return []


def main():

    app = QApplication(sys.argv)
    ex = PacketLogger()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
