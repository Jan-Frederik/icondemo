"""
Initializes the Packets correctly from the JSON and thus
separates this from the GUI itself. This can be used as a library
to return 'Packet' objects read from a packets.json file.

This requires/assumes the packets.json file as the only one that can be changed
on a per-test basis. The other files (packet_contents.json, payloads.json,
and payload_contents.json) are formalized, and are better left alone once defined.

The payload is also opened up but this would preferably remain unused.

Better standards for better coding (sorry)
"""
import json
from struct import Struct, calcsize

PACKET_HEADER_STRUCT = Struct('>B')
PAYLOAD_HEADER_STRUCT = Struct('>BB')
RX_DIAGNOSTICS_CONTENTS = ["RX_DIAGNOSTICS"]  # fill with content like currently done but with split
DEFINITIONS_FOLDER = "packet_definitions/"


def get_packet_definitions(filename=None):
    if filename is None:
        filename = 'packets.json'
    with open(DEFINITIONS_FOLDER + filename, 'r') as fp:
        return json.load(fp)


def get_packet_contents():
    with open(DEFINITIONS_FOLDER + 'packet_contents.json', 'r') as fp:
        return json.load(fp)


def get_payload_definitions(filename=None):
    if filename is None:
        filename = 'payloads.json'
    with open(DEFINITIONS_FOLDER + filename, 'r') as fp:
        return json.load(fp)


def get_payload_contents():
    with open(DEFINITIONS_FOLDER + 'payload_contents.json', 'r') as fp:
        return json.load(fp)


class PacketContents(object):

    def __init__(self, name, metadata, content_metadata):
        self.name = name
        self.identifier = metadata['identifier']

        self.contents = metadata['contents']
        self.description = metadata['description']

        self.construct_packet_data_structs(content_metadata)

    def construct_data_struct(self, content_metadata):
        self.data = '>'
        for content in self.contents:
            self.data += content_metadata[content]
        self.struct = Struct(self.data)
        self.length = self.struct.size


class Packet(PacketContents):
    """Defines a packet, with its data size and format"""

    def __init__(self, name, metadata, content_metadata):
        super(Packet, self).__init__(name, metadata, content_metadata)
        self.log_file = metadata['log_file']

    def construct_data_struct(self, content_metadata):
        # maybe rename to packet_contents_definitions
        super(Packet, self).construct_data_struct(content_metadata)

        self.contents_diag = RX_DIAGNOSTICS_CONTENTS + self.contents
        self.data_diag = '>'
        for content in self.contents_diag:
            self.data_diag += packet_contents[content]
        self.struct_diag = Struct(self.data_diag)
        self.length_diag = self.struct.size


class Payload(PacketContents):
    """Defines a payload, with its data size and format"""
    pass


def sort_packets(packets):
    sorted_packets = [None] * 128
    for packet in packets:
        sorted_packets[packet.identifier] = packet
    return sorted_packets


def sort_payloads(payloads):
    sorted_payloads = [None] * 256
    for payload in payloads:
        sorted_payloads[payload.identifier] = payload
    return sort_payloads


def get_packets(packets_filename=None, payloads_filename=None):
    packet_definitions = get_packet_definitions(packets_filename)
    packet_contents = get_packet_contents()
    packets = []
    for packet_definition in packet_definitions:
        packets.append(Packet(packet_definition, packet_definitions[
                       packet_definition], packet_contents))
    return sort_packets(packets)


def get_payloads(filename=None):
    # idea = remake to fit versions in there somewhere as well
    payload_definitions = get_payload_definitions(filename)
    payload_contents = get_payload_contents()
    payloads = []
    for payload_definition in payload_definitions:
        payloads.append(Payload(payload_definition, payload_definitions[
                        payload_definition], payload_contents))
    return sort_payloads(payloads)


if __name__ == '__main__':
    packets = get_packets()
    payloads = get_payloads()
    for packet in packets:
        print(packet.name, packet.identifier, packet.contents, packet.data)
    for payload in payloads:
        print(payload.name, payload.identifier)
