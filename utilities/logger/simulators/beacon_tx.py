import socket
import struct
from random import randint
from time import sleep, time

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
ip = "192.168.1.4"
ip = "127.0.0.1"

struc = struct.Struct(">BBHQB")

freq_total = 1600
time_delay = 1.0 / freq_total

firmware_version = 0x10
packet_type = 0x0

anchor_ids = [0x6001, 0x6002, 0x6003, 0x6004]

i = 0
while True:
    i += 1
    t = int(time() * 1e07)
    beacon_index = int(i / 4)
    # firmware 1.0, packet type = 0
    data = struc.pack(firmware_version, packet_type, anchor_ids[i % 4], t, beacon_index % 255)
    s.sendto(data, (ip, 8888))
    sleep(time_delay)
