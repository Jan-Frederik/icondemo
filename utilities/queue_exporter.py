import pickle
from time import time
import numpy as np

DURATION_PER_FILE = 5*60

class QueueExporter (object):

    def __init__(self, input_queue, export_path, export_filename):
        self.input_queue = input_queue
        self.export_path = export_path
        self.export_filename = export_filename

    def start(self, export_time):
        i=-1
        n_i = int(np.floor(export_time/DURATION_PER_FILE))
        for i in range(n_i):
            self.record(DURATION_PER_FILE, str(i))
        last_duration = export_time%DURATION_PER_FILE
        self.export_queue(last_duration, str(i+1))

    def export_queue(self, duration, index_name):
        start_time = time()
        # get queue elements and append them to queue_element_list, together with their wait time
        queue_element_list = []
        previous_time = time()
        while time()<start_time+duration:
            queue_element = self.input_queue.get()
            current_time = time()
            queue_element_list.append((current_time-previous_time, queue_element))
            previous_time = current_time
        # pickle save queue_element_list
        with open(self.export_path + self.export_filename + index_name, 'wb') as f:
            pickle.dump(queue_element_list, f)

def run_queue_exporter(export_time, input_queue, export_filename, export_path='test_data/', verbose=False):
    qe = QueueExporter(input_queue, export_path, export_filename)
    if verbose:
        print('*** START RECORDING QUEUE ***')
    qe.start(export_time)
    if verbose:
        print('*** FINISHED RECORDING QUEUE AND SAVED IT AWAY ***')
