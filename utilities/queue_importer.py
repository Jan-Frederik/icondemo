import pickle
import os
from time import time, sleep

class QueueImporter (object):

    def __init__(self, import_path, import_filename, output_queue):
        self.import_path = import_path
        self.import_filename = import_filename
        self.output_queue = output_queue

    def start(self, time_delay):
        i = 0
        while(os.path.exists(self.import_path + self.import_filename + str(i))):
            self.import_queue(time_delay, str(i))
            i += 1

    def import_queue(self, time_delay, index_name):
        # load queue_element_list form pickle file
        with open(self.import_path + self.import_filename + index_name, 'rb') as f:
            queue_element_list = pickle.load(f)
        # put queue elements on queue, after waiting for a time of time_delay (if time_delay is not specified, wait for the saved time period)
        while len(queue_element_list)>0:
            queue_element = queue_element_list.pop(0)
            if time_delay is None:
                sleep(queue_element[0])
            else:
                sleep(time_delay)
            self.output_queue.put(queue_element[1])

def run_queue_importer(output_queue, import_filename, import_path='test_data/', time_delay=None, verbose=False):
    qi = QueueImporter(import_path, import_filename, output_queue)
    if verbose:
        print('*** START IMPORTING FROM FILE ***')
    qi.start(time_delay)
    if verbose:
        print('*** FINISHED IMPORTING FROM FILE AND ADDING TO QUEUE ***')
